-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2018 at 11:09 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nsu_help_desk`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Testname', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

DROP TABLE IF EXISTS `communications`;
CREATE TABLE `communications` (
  `id` int(11) NOT NULL,
  `communication_type` int(11) NOT NULL,
  `author` varchar(500) NOT NULL,
  `start_date` date NOT NULL,
  `stop_date` date NOT NULL,
  `heading` varchar(500) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: not acknowledge, 2: acknowledge',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communications`
--

INSERT INTO `communications` (`id`, `communication_type`, `author`, `start_date`, `stop_date`, `heading`, `message`, `status`, `created`, `modified`) VALUES
(1, 0, 'test12323', '2018-07-19', '2018-07-19', 'Test Header', 'Test', 1, '2018-07-19 06:10:04', '2018-07-19 06:10:04');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `description`, `created`) VALUES
(1, 'testdep1', 'test', '0000-00-00 00:00:00'),
(2, 'testdept2', 'test', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `userid`, `title`, `message`, `created`) VALUES
(1, 1, 'test this one', 'testLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.', '2018-08-13 08:24:30'),
(2, 1, 'mybookmark', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.', '2018-08-13 08:24:33'),
(3, 5, 'mybookmark', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2018-08-13 08:25:42'),
(4, 3, 'How to use', 'For performance reasons, all icons require a base class and individual icon class. To use, place the following code just about anywhere. Be sure to leave a space between the icon and text for proper padding.', '2018-08-13 08:27:22'),
(5, 3, 'tesssdf', 'm dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco l', '2018-08-13 08:27:26'),
(6, 5, 'tesdff', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.', '2018-08-13 08:25:31'),
(7, 3, 'mybookmark', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo ', '2018-08-13 08:26:09'),
(8, 3, 'test this one', 'You can share this', '2018-08-13 08:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `ictservices`
--

DROP TABLE IF EXISTS `ictservices`;
CREATE TABLE `ictservices` (
  `id` int(11) NOT NULL,
  `ri_requesting_office_personel` varchar(225) NOT NULL,
  `ri_college_unit` varchar(225) NOT NULL,
  `ri_request_date` date NOT NULL,
  `ri_time_received` time NOT NULL,
  `ri_receiving_ict_heldesk_personel` varchar(225) NOT NULL,
  `ri_item_received` varchar(225) NOT NULL,
  `ri_item_serial_number` int(20) NOT NULL,
  `ri_contact_number` int(11) NOT NULL,
  `ri_due_on` date NOT NULL,
  `ri_standing_que` varchar(225) NOT NULL,
  `is_description_of_problem` text NOT NULL,
  `is_recomended_solution` text NOT NULL,
  `is_particular` text NOT NULL,
  `is_expected_finish` datetime NOT NULL,
  `is_other_services` text NOT NULL,
  `is_assigned_ict_personel` varchar(225) NOT NULL,
  `is_endorsement` varchar(225) NOT NULL,
  `sr_accomplish_date` varchar(50) NOT NULL,
  `sr_time` varchar(50) NOT NULL,
  `sr_finding` int(11) NOT NULL,
  `sr_findings_others` varchar(225) NOT NULL,
  `sa_date_recieved` int(11) NOT NULL,
  `sa_time` varchar(50) NOT NULL,
  `sa_check_appropriate` int(11) NOT NULL,
  `sa_remarks` varchar(225) NOT NULL,
  `er_date_received` varchar(50) NOT NULL,
  `er_remarks` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ictservices`
--

INSERT INTO `ictservices` (`id`, `ri_requesting_office_personel`, `ri_college_unit`, `ri_request_date`, `ri_time_received`, `ri_receiving_ict_heldesk_personel`, `ri_item_received`, `ri_item_serial_number`, `ri_contact_number`, `ri_due_on`, `ri_standing_que`, `is_description_of_problem`, `is_recomended_solution`, `is_particular`, `is_expected_finish`, `is_other_services`, `is_assigned_ict_personel`, `is_endorsement`, `sr_accomplish_date`, `sr_time`, `sr_finding`, `sr_findings_others`, `sa_date_recieved`, `sa_time`, `sa_check_appropriate`, `sa_remarks`, `er_date_received`, `er_remarks`) VALUES
(1, 'TestName', 'Test College', '2018-07-19', '08:06:00', 'Test Personel', 'Test', 1, 1, '2018-07-19', 'Test', 'Test', 'Test', '', '0000-00-00 00:00:00', 'Test', 'Test', 'Test', 'Test', 'Test', 1, 'Test', 1, 'Test', 1, 'Test', 'Test', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
CREATE TABLE `offices` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `name`, `description`, `created`) VALUES
(1, 'testoffice1', 'test', '2018-07-24 07:34:13'),
(2, 'testoffice2', 'test', '2018-07-24 07:34:13');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `created`) VALUES
(1, 'Administrator', '2018-08-06 01:42:56'),
(2, 'MIS', '2018-08-06 01:43:35'),
(3, 'EGSO', '2018-08-06 01:43:35'),
(4, 'User\r\n', '2018-08-06 01:43:54');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `description`) VALUES
(1, 'Hardware Services', 'Hardware Services'),
(2, 'Software Services', 'Software Services'),
(3, 'Consultiong Services', 'Consulting Services');

-- --------------------------------------------------------

--
-- Table structure for table `services_category`
--

DROP TABLE IF EXISTS `services_category`;
CREATE TABLE `services_category` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siasservices`
--

DROP TABLE IF EXISTS `siasservices`;
CREATE TABLE `siasservices` (
  `id` int(11) NOT NULL,
  `ri_requesting_office_personnel` varchar(225) NOT NULL,
  `ri_college_unit` varchar(225) NOT NULL,
  `ri_date` date NOT NULL,
  `ri_time_received` time NOT NULL,
  `ssr_entry` text NOT NULL,
  `ssr_specify_request` text NOT NULL,
  `sa_date_requested` date NOT NULL,
  `sa_time` time NOT NULL,
  `sa_rate` int(11) NOT NULL,
  `sa_remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `techgroups`
--

DROP TABLE IF EXISTS `techgroups`;
CREATE TABLE `techgroups` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `techgroups`
--

INSERT INTO `techgroups` (`id`, `name`, `description`, `icon`) VALUES
(1, 'MIS', 'MIS', ''),
(2, 'EGSO', 'EGSO', '');

-- --------------------------------------------------------

--
-- Table structure for table `technicals`
--

DROP TABLE IF EXISTS `technicals`;
CREATE TABLE `technicals` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `groups` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `technicals`
--

INSERT INTO `technicals` (`id`, `name`, `groups`, `description`) VALUES
(1, '1', 1, 'testtech1'),
(2, '2', 1, 'testtech2'),
(3, '2', 2, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `requesting_office` int(11) NOT NULL,
  `requesting_personel` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `assign_tech` text NOT NULL,
  `tech_group` int(11) NOT NULL,
  `ticket_type` int(11) NOT NULL,
  `request_type` int(11) NOT NULL,
  `request_category` int(11) NOT NULL,
  `category_type` int(11) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `files` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `requesting_office`, `requesting_personel`, `department`, `assign_tech`, `tech_group`, `ticket_type`, `request_type`, `request_category`, `category_type`, `subject`, `description`, `files`, `status`, `created`, `modified`) VALUES
(1, 1, 1, 1, '1', 2, 1, 1, 1, 1, 'test', 'test', '', 1, '2018-07-24 06:50:16', '2018-07-24 06:50:16'),
(2, 2, 2, 1, '1', 2, 0, 0, 0, 0, '', '', '', 1, '2018-07-24 08:43:09', '2018-07-24 08:43:09'),
(3, 2, 3, 2, '1', 2, 0, 0, 0, 0, '', '', '', 3, '2018-07-24 08:43:17', '2018-07-24 08:43:17'),
(4, 1, 1, 1, '1', 2, 0, 0, 0, 0, '', '', '', 2, '2018-07-24 08:44:19', '2018-07-24 08:44:19'),
(5, 1, 1, 1, '1', 2, 0, 0, 0, 0, '', '', '', 1, '2018-07-24 08:44:33', '2018-07-24 08:44:33'),
(6, 2, 1, 1, '1', 0, 0, 0, 0, 0, '', '', '', 1, '2018-07-24 08:46:04', '2018-07-24 08:46:04'),
(7, 2, 2, 2, '1', 0, 0, 0, 0, 0, '', '', '', 2, '2018-07-24 08:48:51', '2018-07-24 08:48:51'),
(8, 1, 1, 1, '1', 0, 0, 0, 0, 0, '', '', '', 3, '2018-07-24 09:13:32', '2018-07-24 09:13:32'),
(9, 1, 5, 1, '1', 0, 0, 0, 0, 0, '', '', '', 0, '2018-07-24 09:23:22', '2018-07-24 09:23:22'),
(10, 2, 2, 1, '2', 0, 0, 0, 0, 0, '', '', '', 0, '2018-07-24 09:23:34', '2018-07-24 09:23:34'),
(11, 1, 3, 1, '3', 3, 0, 1, 0, 0, 'How are you test subject', 'How are you test subject', '', 5, '2018-07-24 09:23:41', '2018-08-13 03:42:21'),
(12, 1, 2, 2, '4', 0, 0, 0, 0, 0, '', '', '', 0, '2018-07-25 00:09:51', '2018-07-25 00:09:51'),
(13, 1, 1, 1, '2', 0, 0, 0, 0, 0, '', '', '', 0, '2018-07-25 00:18:16', '2018-07-25 00:18:16'),
(14, 1, 1, 1, '1', 0, 0, 0, 0, 0, '', '', '', 0, '2018-07-25 00:18:54', '2018-07-25 00:18:54'),
(15, 1, 1, 1, '5', 0, 0, 0, 0, 0, '', '', '', 0, '2018-07-25 00:19:26', '2018-07-25 00:19:26'),
(16, 1, 2, 1, '2', 0, 0, 0, 0, 0, '', '', '', 0, '2018-07-26 00:01:24', '2018-07-26 00:01:24'),
(17, 2, 1, 1, '2', 0, 0, 0, 0, 0, 'Test', 'Test', '', 0, '2018-07-26 00:09:14', '2018-07-26 00:09:14'),
(18, 1, 1, 2, '3', 0, 0, 0, 0, 0, 'test', 'test', '', 0, '2018-07-26 00:21:15', '2018-07-26 00:21:15'),
(19, 2, 1, 2, '2', 0, 0, 0, 0, 0, 'test', 'test', '', 0, '2018-07-26 07:17:09', '2018-07-26 07:17:09'),
(20, 1, 4, 2, '4', 0, 0, 0, 0, 0, 'yrdydfdgd', 'ffgfgdrf dfgdg', '', 0, '2018-07-26 07:19:54', '2018-07-26 07:19:54'),
(21, 2, 3, 2, '3', 0, 0, 0, 0, 0, 'tsfdsf', 'sdfsf', '', 0, '2018-07-26 07:22:27', '2018-07-26 07:22:27'),
(22, 2, 4, 2, '3', 0, 0, 0, 0, 0, 'tesfs', 'sdfsdf sdfsfdfsdf', '', 3, '2018-07-26 07:24:51', '2018-07-26 07:24:51'),
(23, 2, 4, 2, '4', 0, 0, 0, 0, 0, 'test', 'dd dddddd dddd', '', 2, '2018-07-26 07:25:35', '2018-07-26 07:26:31'),
(24, 2, 2, 2, '2', 2, 0, 0, 0, 0, 'Test', 'High', '', 0, '2018-07-27 16:39:11', '2018-07-27 16:39:11'),
(25, 2, 3, 2, '5', 2, 1, 0, 0, 0, 'test', 'test', '', 2, '2018-07-27 17:25:26', '2018-07-27 17:25:26'),
(26, 1, 2, 1, '2', 2, 1, 2, 0, 0, 'Test', 'test', 'files/153291065536674204_1865702606829255_7227058043534442496_n.jpg', 3, '2018-07-30 00:30:55', '2018-07-30 00:30:55'),
(27, 1, 1, 2, '2', 2, 2, 2, 0, 0, 'ffffff', 'fffff', '', 2, '2018-08-08 07:22:04', '2018-08-08 07:22:04'),
(28, 1, 3, 2, '2', 2, 1, 2, 0, 0, 'test', 'test', '', 1, '2018-08-09 03:01:33', '2018-08-09 03:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `firstname` varchar(225) NOT NULL,
  `lastname` varchar(225) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `department` int(11) NOT NULL,
  `photo` varchar(225) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `role`, `department`, `photo`, `created`, `modified`) VALUES
(1, 'Admin Name', 'Admin', 'Name', 'admin1@test.com', '$2y$10$gbZiE/L3DAri2w.ZgbFdsOY6K.iNyI/frdnXs7Gx3OhzTthbzFw5m', '1', 0, '', '2018-07-03 23:59:38', '2018-07-03 23:59:38'),
(2, 'Admin Name', 'Admin2', 'Name', 'testme@test.com', '$2y$10$gbZiE/L3DAri2w.ZgbFdsOY6K.iNyI/frdnXs7Gx3OhzTthbzFw5m', '1', 0, '', '2018-07-04 01:32:30', '2018-07-04 01:32:30'),
(3, 'User Name', 'User', 'Name', 'user1@test.com', '$2y$10$4kKHt7Y8SipD8lkkGQLP0uxDiaJL4PQ9acZ9W5cI4nn/3v3/UatGS', '4', 0, 'photos/1534140087default.jpg', '2018-07-04 01:54:41', '2018-08-13 06:01:27'),
(4, 'Admin Name', 'test', 'test2', 'usertest@gmail.com', '$2y$10$gbZiE/L3DAri2w.ZgbFdsOY6K.iNyI/frdnXs7Gx3OhzTthbzFw5m', '4', 1, '', '2018-07-04 02:05:48', '2018-07-04 02:05:48'),
(5, 'MIS', 'MIUS', 'Lname', 'mis1@test.com', '$2y$10$4kKHt7Y8SipD8lkkGQLP0uxDiaJL4PQ9acZ9W5cI4nn/3v3/UatGS', '2', 0, '', '2018-07-06 00:07:22', '2018-07-06 00:07:22'),
(6, 'ESGO', 'ESGO', 'Lname', 'admin123@gmail.com', '$2y$10$c9P2Bsq0wDZ0s02KAXby.etyzG9f.6e3yH7TcWsZ7qkvA6vuhMeSC', '3', 0, '', '2018-08-06 01:15:41', '2018-08-06 01:15:41'),
(7, 'test', 'test', 'test', 'testme123@test.com', '$2y$10$bAo9vwLyl3IXXqRVhV3CleclyBVG6f1PcqL3lNnrcMd26BXK4vNqq', '4', 0, '', '2018-08-06 01:19:13', '2018-08-06 01:19:13'),
(8, '', 'testname', 'testname', 'testname@test.com', '$2y$10$c.OZbHgjY8VzriH7rs6tVuvBY3jST2ZVgjnxjva/Fa/oOpecVRupC', '4', 1, '', '2018-08-06 01:29:16', '2018-08-06 01:29:16'),
(9, '', 'ddds', 'sssd', 'admin1@test.com', '$2y$10$3RvzDd7Uf8mHU3vU7FZdQ.868uaHQscBfiKYKRv9G9ekqI6Wn67xO', '1', 2, '', '2018-08-06 01:52:52', '2018-08-06 01:52:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ictservices`
--
ALTER TABLE `ictservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_category`
--
ALTER TABLE `services_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siasservices`
--
ALTER TABLE `siasservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `techgroups`
--
ALTER TABLE `techgroups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `technicals`
--
ALTER TABLE `technicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ictservices`
--
ALTER TABLE `ictservices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services_category`
--
ALTER TABLE `services_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siasservices`
--
ALTER TABLE `siasservices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `techgroups`
--
ALTER TABLE `techgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `technicals`
--
ALTER TABLE `technicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
