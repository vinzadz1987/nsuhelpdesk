<div class="col-md-12" >

<div class="card">
    <div class="card-body">
	<div class="card-body">
	    <div class="text-center">
	        <h3>Add User</h3>
	    </div>
	    <div class="row m-t-20">
        <?= $this->Form->create(null,['url'=> ['controller' => 'Tickets','action' => 'adduser'],'style' => 'width: 285px;margin-right: auto; margin-left: auto;']) ?>
				 <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-user"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('firstname',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Username', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Firstname', 'required' => true]);
		            ?>
	            </div>
				 <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-user"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('lastname',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Username', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Lastname', 'required' => true]);
		            ?>
	            </div>
	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('email',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Username', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Email Address', 'required' => true]);
		            ?>
	            </div>

	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-pencil"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('password',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Password', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Password', 'required' => true]);
		            ?>
		            <?php
		                echo $this->Form->control('role',['value' => '4', 'type' => 'hidden']);
		            ?>
	            </div>

				 <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-pencil"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('confirm_password',['class' => 'form-control form-control-lg','label' => false,'type' => 'password', 'area-label' => 'Password', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Confirm Password', 'required' => true]);
		            ?>
	            </div>

	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
					<?php
		                echo $this->Form->control('position',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Username', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Enter Position', 'required' => true]);
		            ?>
	            </div>

				<div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
					<?php
		                echo $this->Form->control('contact_number',['class' => 'form-control form-control-lg','label' => false, 'type' => 'number', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Contact Number', 'required' => true]);
		            ?>
	            </div>

	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('department',[
		                	'options' => $departments,
		                	'empty' => 'Select Department',
		                	'class' => 'form-control form-control-lg',
		                	'label' => false,
		                	'required' => true
		                ]);
		            ?>
	            </div>
	            <div class="row m-t-20 p-t-20 border-top border-secondary">
	                <div class="col-12">
	                    <?= $this->Form->button(__('<i class="fa fa-check" aria-hidden="true"></i> Save & Exit'),['class' => 'btn btn-info float-right', 'escape' => false]) ?>
                        <?= $this->Html->link(__('<i class="fa fa-undo"></i> Back'),['Controller' => 'Tickets','action' => 'userslist'], ['class' => 'btn btn-danger', 'escape' => false]) ?>
       					<?= $this->Form->end() ?>
	                </div>
	            </div>
	        </form>
	    </div>
	</div>
</div>
</div>
</div>