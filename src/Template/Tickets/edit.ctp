<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #2255A4 !important;
        border: 1px solid #aaa !important;
        border-radius: 4px !important;
        cursor: default !important;
        float: left !important;
        margin-right: 5px !important;
        margin-top: 5px !important;
        padding: 0 5px !important;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="tickets form large-9 medium-8 columns content">
            <?= $this->Form->create($ticket, ['type' => 'file']) ?>
            <fieldset>
            <legend><?= __('Update Request') ?></legend>
            <div class="form-group row">
                <div class="col-md-6">
                    <?= $this->Form->control('requesting_office', [
                        'options' => $offices,
                        'empty' => 'Please Select',
                        'label' => 'Requesting Office/Department',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('requesting_personel', [
                        'options' => $users,
                        'empty' => 'Please Select',
                        'class' => ['select2 form-control'],
                        'value' => isset($sess_user_id)? $sess_user_id : ''
                    ]) ?> 
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('tech_group', [
                        'options' => $techgroups,
                        'empty' => 'Please Select',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('ticket_type', [
                        'options' => $ticket_type,
                        'empty' => 'Please Select',
                        'label' => 'Request Type',
                        'class' => ['select2 form-control']
                    ]) ?>
                    <small>
                        <span class="badge badge-pill badge-warning">Critical</span>
                        <span class="badge badge-pill badge-danger">Urgent</span>
                        <span class="badge badge-pill badge-danger">High</span>
                        <span class="badge badge-pill badge-primary">Medium</span>
                        <span class="badge badge-pill badge-success">Low</span>
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <?= $this->Form->control('request_type', [
                        'options' => $services,
                        'empty' => 'Please Select',
                        'label' => 'Services',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
                <div class="col-md-6">
                     <?= $this->Form->control('request_category', [
                        'options' => $serviceCategory,
                        'empty' => 'Please Select',
                        'label' => 'Services Types',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <?php
                         echo $this->Form->control('subject',['class' => 'form-control']);
                    ?>
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?php
                        echo $this->Form->control('description',['class' => 'form-control', 'label' => 'Request Details']);
                    ?>
                </div>
            </div> 
             <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('files', ['class' => 'form-control', 'type' => 'file', 'label' => 'Attachment']) ?>
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('status', [
                        'options' => $ticket_status,
                        'empty' => 'Please Select',
                        'class' => ['form-control select2']
                    ]) ?>
                    <small>
                        <span class="badge badge-pill badge-primary">New</span>
                        <span class="badge badge-pill badge-info">Open</span>
                        <span class="badge badge-pill badge-danger">Pending</span>
                        <span class="badge badge-pill badge-primary">In Progress</span>
                        <span class="badge badge-pill badge-success">Solved</span>
                        <span class="badge badge-pill badge-danger">Closed</span>
                    </small>
                </div>
            </div> 
            </fieldset>
            <?= $this->Form->button(__('<i class="fa fa-save"></i> Save & Exit'), ['class' => 'btn btn-primary', 'escape' => false]) ?>
            <?= $this->Html->link(__('<i class="fa fa-undo"></i> Back'),['action' => 'index'], ['class' => 'btn btn-danger', 'escape' => false]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<?= $this->Html->script([
    'http://code.jquery.com/jquery-1.9.1.js'
]) ?>
<script type="text/javascript">
	$(function () {
		$("#tech-group").change(function () {
			getTechnicals($(this).val());     
		});
	});
	function getTechnicals(tg) {
		$.ajax({
			type: 'post',
			async: true,
			cache: false,
			url:'get_by_group',
			success: function (response) {
				console.log(response);
			},
			error: function(response) {
				toastr.error('Something error', 'Error!');
			},
			data: {group: tg}
		});
	}

</script>
