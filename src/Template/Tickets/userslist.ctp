
<div class="card">
    <div class="card-body">
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
    <div style="padding: 5px">
        <?php
            echo $this->Html->link(
            '<i class="fas fa-plus"></i> Add User',
            [
                'controller' => 'Tickets',
                'action' => 'adduser'
            ],
            [ 
                'escape' => false,
                'class' => 'btn btn-primary btn-md'
            ]
            )
        ?>
    </div>
    <?php // $this->Html->link(__('New User'), ['action' => 'add']) ?>
    <table cellpadding="0" cellspacing="0" class="table" id="usersTable">
        <thead>
            <tr>
                <!-- <th scope="col">id</th> -->
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Department</th>
                <th>Position</th>
                <th>Contact Number</th>
                <th>Created</th>
                <th>Modified</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->firstname) ?> <?= h($user->lastname) ?></td>
                <td><?= h($user->email) ?></td>
                <!-- <td><?= h($user->password) ?></td> -->
                <td>
                    <?php 
                        if(h($user->role) == 1) {
                            echo 'Administrator';
                        } else if (h($user->role) == 2) {
                            echo 'MIS';
                        } else if (h($user->role) == 3) {
                            echo 'EGSO';
                        } else {
                            echo 'User';
                        }
                    ?>
                </td>
                <td><?= h($user->user_department['name']) ?></td>
                <td><?= h($user->position); ?></td>
                <td><?= h($user->contact_number); ?></td>
                <td><?= h($user->created) ?></td>
                <td><?= h($user->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div> -->
</div>
</div>
</div>
