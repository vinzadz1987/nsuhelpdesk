<div class="table-responsive">
    <div class="tickets index large-9 medium-8 columns content" style="background-color:white; padding: 20px ">
        <h3><?= __('History') ?></h3>
        <table id="tickets" class="table table-responsive">
            <thead>
                <tr>
                    <th>Requesting Office/Department</th>
                    <th>Requesting Personel</th>
                    <th>Technical Group</th>
                    <th>Request Type</th>
                    <th>Services</th>
                    <th>Severity</th>
                    <th>Subject</th>
                    <th>Request Details</th>
                    <th>Attachment</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tickets as $ticket): ?>
                <tr>
                    <td><?= $offices[$this->Number->format($ticket->requesting_office)] ?></td>
                    <td>
                        <?= $users[$this->Number->format($ticket->requesting_personel)] ?>
                    </td>
                    <td>
                        <?php 
                            if($ticket->tech_group !== 0) {
                                echo $techgroups[$this->Number->format($ticket->tech_group)];
                            }
                        ?>  
                    </td>
                    <td><?= $typeTable[$this->Number->format($ticket->ticket_type)] ?></td>
                    <td>
                        <?php 
                            if($ticket->request_type !== 0) {
                                echo $services[$this->Number->format($ticket->request_type)];
                            }
                        ?>  
                    </td>
                    <td>
                        <?php 
                            if($ticket->request_category !== 0) {
                                echo $serviceCategory[$this->Number->format($ticket->request_category)];
                            }
                        ?>  
                    </td>
                    <td><?= h($ticket->subject) ?></td>
                    <td><?= h($ticket->description) ?></td>
                    <td><img src="<?php echo $this->request->getAttribute("webroot") .h($ticket->files); ?>" style="height: 100px;" /> 
                        <?php 
                            // if($ticket->files !== "") {
                            //     echo $this->Html->Link(substr(explode('/',h($ticket->files))[1], 0, 10), ['controller' => 'Tickets', 'action' => 'download', $ticket->files]);
                            // }
                        ?>
                    <td><?= $ticketTable[$this->Number->format($ticket->status)] ?></td>
                    <td><?= h($ticket->created) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('<i class="fa fa-print"></i>'), ['action' => 'view', $ticket->id], ['class' => 'text-primary', 'escape' => false])?>
                        <?= $this->Html->link(__('<i class="fas fa-edit"></i>'), ['action' => 'edit', $ticket->id], ['class' => 'text-primary', 'escape' => false])?>
                        <?= $this->Form->postLink(__('<i class="fas fa-trash"></i>'), ['action' => 'delete', $ticket->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ticket->id),'class' => 'text-danger','escape' => false]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
