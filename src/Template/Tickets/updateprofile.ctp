<div class="card">
	<div class="card-body">
		<div class="users form large-9 medium-8 columns content">
			<?= $this->Form->create($user, ['class' => 'form-horizontal', 'type' => 'file']) ?>
				<fieldset>
					<legend><?= __('Update Profile') ?></legend>
					<img src="<?php echo $this->request->getAttribute("webroot") .$user['photo']; ?>" class="rounded-circle" style="height: 100px" />
					<?= $this->Form->control('photo', ['type' => 'file', 'label' => 'Choose Photo', 'accept' => 'image/*', 'class' => 'form-control']) ?>
					<?php
						echo $this->Form->control('firstname',['class' => 'form-control form-control-lg']);
						echo $this->Form->control('lastname',['class' => 'form-control form-control-lg']);
						echo $this->Form->control('position',['class' => 'form-control form-control-lg']);
						echo $this->Form->control('contact_number',['class' => 'form-control form-control-lg']);
		                echo $this->Form->control('email',['class' => 'form-control form-control-lg']);
		                echo $this->Form->control('password',['class' => 'form-control form-control-lg']);
					?>
				</fieldset>
				<hr>
				<?= $this->Form->button(__('<i class="fas fa-save"></i> Update'), ['class' => 'btn btn-primary', 'escape' => false]) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>
