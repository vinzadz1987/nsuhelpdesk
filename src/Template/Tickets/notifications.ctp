<div class="container-fluid">
	<div class="row">
		
<style>
	.notifications {
  list-style: none;
  padding: 0;
}
.notification {
  display: block;
  padding: 9.6px 12px;
  border-width: 0 0 1px 0;
  border-style: solid;
  border-color: #eeeeee;
  background-color: #fff;
  color: #333333;
  text-decoration: none;
}
.notification:last-child {
  border-bottom: 0;
}
.notification:hover,
.notification.active:hover {
  background-color: #f9f9f9;
  border-color: #eeeeee;
}
.notification.active {
  background-color: #f4f4f4;
}
a.notification:hover {
  text-decoration: none;
}
.notification-title {
  font-size: 15px;
  margin-bottom: 0;
}
.notification-desc {
  margin-bottom: 0;
}
.notification-meta {
  color: #777777;
}
</style>
<div class="col-md-12">
<div class="card">
    <div class="card-body">
<h2>Notification</h2>
<ul class="notifications">
<?php foreach($notif as $noti ) { ?>
  <li class="notification">
      <div class="media">
        <div class="media-left">
          <div class="media-object">
		    <?php if($noti->user_noti->photo == "") { ?>
			
				<img class="rounded-circle" style="height: 30px; width: 30px" src="http://famepeople.alphafm.org/assets/headshot.png" class="img-circle" alt="Name">&nbsp;
				
			<?php } else { ?>

				<img class="rounded-circle" style="height: 30px; width: 30px" src="<?php echo $this->request->getAttribute("webroot") . $noti->user_noti->photo; ?>" class="img-circle" alt="Name">

			<?php } ?>
            
          </div>
        </div>
        <div class="media-body">
          <strong class="notification-title"><a href="#"><?php echo ucfirst($noti->user_noti->firstname); ?> <?php echo ucfirst($noti->user_noti->lastname); ?></a> Added <a href="#">New Request </a></strong> from <strong class="notification-title"> <?php echo ucfirst($noti->request_office->name); ?> </strong>
          <!-- <p class="notification-desc">I totally don't wanna do it. Rimmer can do it.</p> -->
		  <button class="btn btn-primary" style="float:right">Acknowledge</button>
          <div class="notification-meta">
            <small class="timestamp">27. 11. 2015, 15:00</small>
          </div>
        </div>
      </div>
  </li>
<?php } ?>
  <!-- <li class="notification">
      <div class="media">
        <div class="media-left">
          <div class="media-object">
            <img data-src="holder.js/50x50?bg=cccccc" class="img-circle" alt="Name">
          </div>
        </div>
        <div class="media-body">
          <strong class="notification-title"><a href="#">Nikola Tesla</a> resolved <a href="#">T-14 - Awesome stuff</a></strong>

          <p class="notification-desc">Resolution: Fixed, Work log: 4h</p>

          <div class="notification-meta">
            <small class="timestamp">27. 10. 2015, 08:00</small>
          </div>

        </div>
      </div>
  </li> -->

  <!-- <li class="notification">
      <div class="media">
        <div class="media-left">
          <div class="media-object">
            <img data-src="holder.js/50x50?bg=cccccc" class="img-circle" alt="Name">
          </div>
        </div>
        <div class="media-body">
          <strong class="notification-title"><a href="#">James Bond</a> resolved <a href="#">B-007 - Desolve Spectre organization</a></strong>

          <div class="notification-meta">
            <small class="timestamp">1. 9. 2015, 08:00</small>
          </div>

        </div>
      </div>
  </li> -->
</ul>
	</div>
	</div></div>
</div>
</div>
