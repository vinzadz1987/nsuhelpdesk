<style type="text/css">
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #2255A4 !important;
        border: 1px solid #aaa !important;
        border-radius: 4px !important;
        cursor: default !important;
        float: left !important;
        margin-right: 5px !important;
        margin-top: 5px !important;
        padding: 0 5px !important;
    }
</style>
<div class="card" id="DivIdToPrint">
    <div class="card-body">
        <div class="tickets form large-9 medium-8 columns content">
            <?= $this->Form->create($ticket, ['type' => 'file']) ?>
            <fieldset>
            <legend><?= __('View Request') ?></legend>
            <div class="form-group row">
                <div class="col-md-6">
                    <?= $this->Form->control('requesting_office', [
                        'options' => $offices,
                        'empty' => 'Please Select',
                        'label' => 'Requesting Office/Department',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('requesting_personel', [
                        'options' => $users,
                        'empty' => 'Please Select',
                        'class' => ['select2 form-control'],
                        'value' => isset($sess_user_id)? $sess_user_id : ''
                    ]) ?> 
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('tech_group', [
                        'options' => $techgroups,
                        'empty' => 'Please Select',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('ticket_type', [
                        'options' => $ticket_type,
                        'empty' => 'Please Select',
                        'label' => 'Request Type',
                        'class' => ['select2 form-control']
                    ]) ?>
                    <small>
                        <span class="badge badge-pill badge-warning">Critical</span>
                        <span class="badge badge-pill badge-danger">Urgent</span>
                        <span class="badge badge-pill badge-danger">High</span>
                        <span class="badge badge-pill badge-primary">Medium</span>
                        <span class="badge badge-pill badge-success">Low</span>
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <?= $this->Form->control('request_type', [
                        'options' => $services,
                        'empty' => 'Please Select',
                        'label' => 'Services',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
                <div class="col-md-6">
                     <?= $this->Form->control('request_category', [
                        'options' => $serviceCategory,
                        'empty' => 'Please Select',
                        'label' => 'Services Types',
                        'class' => ['select2 form-control']
                    ]) ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <?php
                         echo $this->Form->control('subject',['class' => 'form-control']);
                    ?>
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?php
                        echo $this->Form->control('description',['class' => 'form-control', 'label' => 'Request Details']);
                    ?>
                </div>
            </div> 
             <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('files', ['class' => 'form-control', 'type' => 'file', 'label' => 'Attachment']) ?>
                </div>
            </div> 
            <div class="form-group row">
                <div class="col-md-12">
                    <?= $this->Form->control('status', [
                        'options' => $ticket_status,
                        'empty' => 'Please Select',
                        'class' => ['form-control select2']
                    ]) ?>
                    <small>
                        <span class="badge badge-pill badge-primary">New</span>
                        <span class="badge badge-pill badge-info">Open</span>
                        <span class="badge badge-pill badge-danger">Pending</span>
                        <span class="badge badge-pill badge-primary">In Progress</span>
                        <span class="badge badge-pill badge-success">Solved</span>
                        <span class="badge badge-pill badge-danger">Closed</span>
                    </small>
                </div>
            </div> 
            </fieldset>
            <?= $this->Form->button(__('<i class="fa fa-save"></i> Print'), ['class' => 'btn btn-primary', 'escape' => false, 'onclick' => 'printDiv()']) ?>
            <?= $this->Html->link(__('<i class="fa fa-undo"></i> Back'),['action' => 'index'], ['class' => 'btn btn-danger', 'escape' => false]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<?= $this->Html->script([
    'http://code.jquery.com/jquery-1.9.1.js'
]) ?>
<script type="text/javascript">
	$(function () {
		$("#tech-group").change(function () {
			getTechnicals($(this).val());     
		});
	});
	function getTechnicals(tg) {
		$.ajax({
			type: 'post',
			async: true,
			cache: false,
			url:'get_by_group',
			success: function (response) {
				console.log(response);
			},
			error: function(response) {
				toastr.error('Something error', 'Error!');
			},
			data: {group: tg}
		});
	}

    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

</script>




<!-- <div class="card">
    <div class="card-body">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ticket'), ['action' => 'edit', $ticket->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ticket'), ['action' => 'delete', $ticket->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ticket->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tickets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ticket'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tickets view large-9 medium-8 columns content">
    <h3><?= h($ticket->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Subject') ?></th>
            <td><?= h($ticket->subject) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ticket->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Department') ?></th>
            <td><?= $this->Number->format($ticket->department) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assign Tech') ?></th>
            <td><?= $this->Number->format($ticket->assign_tech) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tech Group') ?></th>
            <td><?= $this->Number->format($ticket->tech_group) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ticket Type') ?></th>
            <td><?= $this->Number->format($ticket->ticket_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Request Type') ?></th>
            <td><?= $this->Number->format($ticket->request_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Request Category') ?></th>
            <td><?= $this->Number->format($ticket->request_category) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category Type') ?></th>
            <td><?= $this->Number->format($ticket->category_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($ticket->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($ticket->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($ticket->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($ticket->description)); ?>
    </div>
</div>
</div>
</div> -->



