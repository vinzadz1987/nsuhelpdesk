
<div class="container-fluid" style="background: rgba(0, 0, 0, 0.6); padding: 110px;">
	<div class="title" style="color: white;"><h4> Request Type</h3></div>
	<hr>

	<div class="row">
		<?php foreach($ticketType as $ticket) { ?>
			<?php $ticket_type = ['Critical','Urgent','High','Medium','Low']?>
			<?php $box_type = ['bg-warning','bg-info','bg-danger','bg-primary','bg-success']?>
			<?php $tt = $ticket->ticket_type; ?>
			<?php $c = $ticket->count; ?>
			<?= $this->Html->link(__(
				'<div>
					<div class="card card-hover">
						<div class="box '.$box_type[$tt].' text-center">
							<h1 class="font-light text-white">'.$c.'</h1>
							<h6 class="text-white">'.$ticket_type[$tt].'</h6>
						</div>
					</div>
				</div>'
			), ['controller' => 'faqs','action' => 'add'], ['class' => 'col-md-6 col-lg-2 col-xlg-3','escape' => false])?>
		<?php  } ?>
	</div>

	<div class="title" class="title" style="color: white;"><h4> Status</h3></div>
	<hr>
	<div class="row">
		<?php foreach($status as $stat) { ?>
			<?php $box_type = ['bg-primary','bg-info','bg-danger','bg-primary','bg-success','bg-danger']?>
			<?php $tt = $stat->status; ?>
			<?php $c = $stat->count; ?>
			<?= $this->Html->link(__(
				'<div>
					<div class="card card-hover">
						<div class="box '.$box_type[$tt].' text-center">
							<h1 class="font-light text-white">'.$c.'</h1>
							<h6 class="text-white">'.$ticket_status[$tt].'</h6>
						</div>
					</div>
				</div>'
			), ['controller' => 'faqs','action' => 'add'], ['class' => 'col-md-6 col-lg-2 col-xlg-3','escape' => false])?>
		<?php  } ?>
	</div>

	


<div class="title" class="title" style="color: white;"><h4> Menus</h3></div>
	<hr>
	<div class="row">
<?php //var_dump(); ?>
		<?= $this->Html->link(__(
		 	'<div>
				<div class="card card-hover">
					<div class="box bg-primary text-center">
						<h1 class="font-light text-white"><i class="fa fa-bell"></i></h1>
						<h6 class="text-white">Notification <span class="badge badge-light">'.$Notifications[0]['count'].'</span></h6>
					</div>
				</div>
			</div>'
		 ), ['controller' => 'Tickets','action' => 'notifications'], ['class' => 'col-md-6 col-lg-2 col-xlg-2','escape' => false])?>

		 <?= $this->Html->link(__(
		 	'<div>
				<div class="card card-hover">
					<div class="box bg-danger text-center">
						<h1 class="font-light text-white"><i class="fa fa-question"></i></h1>
						<h6 class="text-white">FAQS</h6>
					</div>
				</div>
			</div>'
		 ), ['controller' => 'faqs','action' => 'add'], ['class' => 'col-md-6 col-lg-2 col-xlg-3','escape' => false])?>

		<?= $this->Html->link(__(
		 	'<div>
				<div class="card card-hover">
					<div class="box bg-primary text-center">
						<h1 class="font-light text-white"><i class="fa fa-history"></i></h1>
						<h6 class="text-white">History</h6>
					</div>
				</div>
			</div>'
		 ), ['controller' => 'Tickets','action' => 'history'], ['class' => 'col-md-6 col-lg-2 col-xlg-2','escape' => false])?>
	</div>
</div>
	</div>
