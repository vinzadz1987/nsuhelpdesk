<div class="container-fluid">
	<div class="row">
		<a href="add" class="col-md-6 col-lg-6 col-xlg-3">
		<div>
				<div class="card card-hover">
					<div class="box bg-success text-center">
						<h1 class="font-light text-white"><i class="fa fa-ticket"></i></h1>
						<h6 class="text-white">New Request</h6>
					</div>
				</div>
			</div>
		</a>
		<div class="col-md-6 col-lg-6 col-xlg-3">
			<div class="card card-hover">
				<div class="box bg-danger text-center">
					<h1 class="font-light text-white"><i class="fa fa-bell"></i></h1>
					<h6 class="text-white">Notification <span class="badge badge-light">4</span></h6>
				</div>
			</div>
		</div>
		
		<?= $this->Html->link(__(
		 	'<div>
				<div class="card card-hover">
					<div class="box bg-info text-center">
						<h1 class="font-light text-white"><i class="fa fa-question"></i></h1>
						<h6 class="text-white">Frequently Asked Questions</h6>
					</div>
				</div>
			</div>'
		 ), ['controller' => 'faqs','action' => 'add'], ['class' => 'col-md-6 col-lg-6 col-xlg-3','escape' => false])?>

		<?php
		//  $this->Html->link(__(
		//  	'<div>
		// 		<div class="card card-hover">
		// 			<div class="box bg-info text-center">
		// 				<h1 class="font-light text-white"><i class="fa fa-users"></i></h1>
		// 				<h6 class="text-white">Departments</h6>
		// 			</div>
		// 		</div>
		// 	</div>'
		//  ), ['controller' => 'departments','action' => 'index'], ['class' => 'col-md-6 col-lg-4 col-xlg-3','escape' => false])
		 ?>

		 <?php
		//   $this->Html->link(__(
		//  	'<div>
		// 		<div class="card card-hover">
		// 			<div class="box bg-info text-center">
		// 				<h1 class="font-light text-white"><i class="fa fa-users"></i></h1>
		// 				<h6 class="text-white">Offices</h6>
		// 			</div>
		// 		</div>
		// 	</div>'
		//  ), ['controller' => 'offices','action' => 'index'], ['class' => 'col-md-6 col-lg-4 col-xlg-3','escape' => false])
		 ?>
		 
		 <?php
		//   $this->Html->link(__(
		//  	'<div>
		// 		<div class="card card-hover">
		// 			<div class="box bg-info text-center">
		// 				<h1 class="font-light text-white"><i class="fa fa-user"></i></h1>
		// 				<h6 class="text-white">Service Provider</h6>
		// 			</div>
		// 		</div>
		// 	</div>'
		//  ), ['controller' => 'Technicals','action' => 'index'], ['class' => 'col-md-6 col-lg-4 col-xlg-3','escape' => false])
		 ?>

		 <?php
		//   $this->Html->link(__(
		//  	'<div>
		// 		<div class="card card-hover">
		// 			<div class="box bg-info text-center">
		// 				<h1 class="font-light text-white"><i class="fa fa-user"></i></h1>
		// 				<h6 class="text-white">Services</h6>
		// 			</div>
		// 		</div>
		// 	</div>'
		//  ), ['controller' => 'Services','action' => 'index'], ['class' => 'col-md-6 col-lg-4 col-xlg-3','escape' => false])
		 ?>


		<?= $this->Html->link(__(
		 	'<div>
				<div class="card card-hover">
					<div class="box bg-primary text-center">
						<h1 class="font-light text-white"><i class="fa fa-history"></i></h1>
						<h6 class="text-white">History</h6>
					</div>
				</div>
			</div>'
		 ), ['controller' => 'Tickets','action' => 'history'], ['class' => 'col-md-6 col-lg-6 col-xlg-3','escape' => false])?>

		 <?php
		//  $this->Html->link(__(
		//  	'<div>
		// 		<div class="card card-hover">
		// 			<div class="box bg-primary text-center">
		// 				<h1 class="font-light text-white"><i class="fa fa-users"></i></h1>
		// 				<h6 class="text-white">Users</h6>
		// 			</div>
		// 		</div>
		// 	</div>'
		//  ), ['controller' => 'Users','action' => 'index'], ['class' => 'col-md-6 col-lg-4 col-xlg-3','escape' => false])
		 ?>
		
		<!-- <div class="col-md-6 col-lg-4 col-xlg-3">
			<div class="card card-hover">
				<div class="box bg-primary text-center">
					<h1 class="font-light text-white"><i class="fa fa-sign-out"></i></h1>
					<h6 class="text-white">Logout</h6>
				</div>
			</div>
		</div> -->
	</div>
</div>
