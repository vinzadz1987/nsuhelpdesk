<style type="text/css">
	#department, #role {
		width: 230px
	}
</style>
<div class="header col-md-12" style="text-align: center;
    display: block;
    font-weight: normal;
    padding: 5px 1.5rem 0 1.5rem;
    transition: height 300ms ease-out 0s;
    background-color: #a0d3e8;
    color: #626262;
    top: 0px;
    z-index: 999;
    overflow: hidden;
    height: 100px;
    line-height: 2.5em;
    position: absolute;
    background: rgba(0, 0, 0, 0.6);
    font-size: 35px;
    font-weight: bold;
    font-family: Impact, Charcoal, sans-serif;
    color: white
    ">
  <?php echo $this->Html->image("nsulogo.png", [
						'class' => 'rounded-circle',
						'style' => 'width: 60px'
					]); ?>  NSU HELP DESK
</div>


<div class="">
	<div class="col-md-12">

	<div style="background: rgba(0, 0, 0, 0.6); padding: 50px;">
	
	    <div class="text-center p-t-20 p-b-20">
	        <span class="db">
	        	<h5 class="small-header">Sign In <span id="accessName"></span></h5>
	        </span>
	    </div>
		<?= $this->Form->create(null, ['style' => 'width: 285px;margin-right: auto;margin-left: auto;']) ?>
	        <div class="row p-b-30">
	            <div class="col-12">
	                <div class="input-group mb-3">
	                    <div class="input-group-prepend">
	                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
	                    </div>
	                    <?= $this->Form->control('email', ['class' => 'form-control form-control-lg', 'placeholder' => 'test@example.com', 'label' => false]) ?>
	                </div>
	                <div class="input-group mb-3">
	                    <div class="input-group-prepend">
	                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
	                    </div>
	                    <?= $this->Form->control('password', ['class'=>'form-control form-control-lg', 'placeholder' => 'Password', 'label' => false]) ?>
	                </div>
	            </div>
	        </div>
	        <div class="row border-top border-secondary">
	            <div class="col-12">
	                <div class="form-group">
	                    <div class="p-t-20">
	                        <?= $this->Form->button('<i class="fa fa-check"></i> Sign In', ['class' => 'btn btn-success float-right', 'escape' => false]) ?>
	                    </div>
	                </div>
	            </div>
	        </div>
	    <?= $this->Form->end() ?>
	</div>
</div>
<!-- <div class="col-md-6" >
	<div style="background: rgba(0, 0, 0, 0.6); padding: 12px;">
	    <div class="text-center">
	        <h3><span class="text-white">Create Account</span></h3>
	    </div>
	    <div class="row m-t-20">
			<?= $this->Form->create(null,['url'=> ['controller' => 'Users','action' => 'add'],'style' => 'width: 285px;margin-right: auto; margin-left: auto;']) ?>
				 <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-user"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('firstname',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Username', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Firstname', 'required' => true]);
		            ?>
	            </div>
				 <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-user"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('lastname',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Username', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Lastname', 'required' => true]);
		            ?>
	            </div>
	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('email',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Username', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Email Address', 'required' => true]);
		            ?>
	            </div>
	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-pencil"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('password',['class' => 'form-control form-control-lg','label' => false, 'area-label' => 'Password', 'aria-describedby' => 'basic-addon1', 'placeholder' => 'Password', 'required' => true]);
		            ?>
		            <?php
		                echo $this->Form->control('role',['value' => 'User', 'type' => 'hidden']);
		            ?>
	            </div>
	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('department',[
		                	'options' => $departments,
		                	'empty' => 'Select Department',
		                	'class' => 'form-control form-control-lg',
		                	'label' => false,
		                	'required' => true
		                ]);
		            ?>
	            </div>
	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
	                <?php
		                echo $this->Form->control('role',[
		                	'options' => $globalRoles,
		                	'empty' => 'Select Position',
		                	'class' => 'form-control form-control-lg',
		                	'label' => false,
		                	'required' => true
		                ]);
		            ?>
	            </div>
	            <div class="row m-t-20 p-t-20 border-top border-secondary">
	                <div class="col-12">
	                    <?= $this->Form->button(__('<i class="fa fa-user-plus" aria-hidden="true"></i> Sign up'),['class' => 'btn btn-info float-right', 'escape' => false]) ?>
       					<?= $this->Form->end() ?>
	                </div>
	            </div>
	        </form>
	    </div>
	</div>
</div> -->
</div>
<?= $this->Html->script(['libs/jquery/dist/jquery.min']) ?>
</div>
 <script>
// ============================================================== 
// Login and Recover Password 
// ============================================================== 
$('#register').on("click", function() {
	$("#loginform").slideUp();
	$("#recoverform").fadeIn();
});
$('#to-login').click(function(){

	$("#recoverform").hide();
	$("#loginform").fadeIn();
});
$( function() {

});
function loginName(ident) {
	console.log(ident);
}
</script>