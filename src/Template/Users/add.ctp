<div class="users form large-9 medium-8 columns content">
    <div class="row m-t-20">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('Add User') ?></legend>
            <?php
                echo $this->Form->control('firstname',['class' => 'form-control form-control-lg']);
                echo $this->Form->control('lastname',['class' => 'form-control form-control-lg']);
                echo $this->Form->control('email',['class' => 'form-control form-control-lg']);
                echo $this->Form->control('password',['class' => 'form-control form-control-lg']);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
