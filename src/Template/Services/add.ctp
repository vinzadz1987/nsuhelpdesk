<div class="services form large-9 medium-8 columns content" style="background-color:white; padding: 20px ">
    <?= $this->Form->create($service) ?>
    <fieldset>
        <legend><?= __('Add Service') ?></legend>
        <?php
            echo $this->Form->control('name',['class' => 'form-control']);
            echo $this->Form->control('description',['class' => 'form-control','style' => 'text-align: left']);
        ?>
    </fieldset>
    <hr>
    
    <?= $this->Form->button(__('Save'),['class' => 'btn btn-primary']) ?>
    <?= $this->Html->link(__('Back'), ['action' => 'index'],['class' => 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>
