<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ictservice $ictservice
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ictservice'), ['action' => 'edit', $ictservice->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ictservice'), ['action' => 'delete', $ictservice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ictservice->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ictservices'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ictservice'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ictservices view large-9 medium-8 columns content">
    <h3><?= h($ictservice->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ri Requesting Office Personel') ?></th>
            <td><?= h($ictservice->ri_requesting_office_personel) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri College Unit') ?></th>
            <td><?= h($ictservice->ri_college_unit) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Receiving Ict Heldesk Personel') ?></th>
            <td><?= h($ictservice->ri_receiving_ict_heldesk_personel) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Item Received') ?></th>
            <td><?= h($ictservice->ri_item_received) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Standing Que') ?></th>
            <td><?= h($ictservice->ri_standing_que) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Assigned Ict Personel') ?></th>
            <td><?= h($ictservice->is_assigned_ict_personel) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Endorsement') ?></th>
            <td><?= h($ictservice->is_endorsement) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sr Accomplish Date') ?></th>
            <td><?= h($ictservice->sr_accomplish_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sr Time') ?></th>
            <td><?= h($ictservice->sr_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sr Findings Others') ?></th>
            <td><?= h($ictservice->sr_findings_others) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sa Time') ?></th>
            <td><?= h($ictservice->sa_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sa Remarks') ?></th>
            <td><?= h($ictservice->sa_remarks) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Er Date Received') ?></th>
            <td><?= h($ictservice->er_date_received) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Er Remarks') ?></th>
            <td><?= h($ictservice->er_remarks) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ictservice->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Item Serial Number') ?></th>
            <td><?= $this->Number->format($ictservice->ri_item_serial_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Contact Number') ?></th>
            <td><?= $this->Number->format($ictservice->ri_contact_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sr Finding') ?></th>
            <td><?= $this->Number->format($ictservice->sr_finding) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sa Date Recieved') ?></th>
            <td><?= $this->Number->format($ictservice->sa_date_recieved) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sa Check Appropriate') ?></th>
            <td><?= $this->Number->format($ictservice->sa_check_appropriate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Request Date') ?></th>
            <td><?= h($ictservice->ri_request_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Time Received') ?></th>
            <td><?= h($ictservice->ri_time_received) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ri Due On') ?></th>
            <td><?= h($ictservice->ri_due_on) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Is Description Of Problem') ?></h4>
        <?= $this->Text->autoParagraph(h($ictservice->is_description_of_problem)); ?>
    </div>
    <div class="row">
        <h4><?= __('Is Recomended Solution') ?></h4>
        <?= $this->Text->autoParagraph(h($ictservice->is_recomended_solution)); ?>
    </div>
    <div class="row">
        <h4><?= __('Is Other Services') ?></h4>
        <?= $this->Text->autoParagraph(h($ictservice->is_other_services)); ?>
    </div>
</div>
