<style type="text/css">
    .formTitle {
        background-color: gray; padding: 15px;color: black; 
    }
</style>
 <div class="card">
    <div class="card-body">
         <?= $this->Form->create($ictservice) ?>
        <h5 class="card-title text-center formTitle"><b>1: REQUESTER INFORMATION</b></h5>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Requesting Office/Personel</label>
                <?php echo $this->Form->control('ri_requesting_office_personel', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">College/Unit</label>
                <?php echo $this->Form->control('ri_college_unit', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Date</label>
                <?php echo $this->Form->control('ri_request_date', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Time Received</label>
                <?php echo $this->Form->control('ri_time_received', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-md-6 m-t-15">Receiving ICT Helpdesk Personel</label>
                <?php echo $this->Form->control('ri_receiving_ict_helpdesk_personel', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Item Received</label>
                <?php echo $this->Form->control('ri_item_received', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Item Serial Number</label>
                <?php echo $this->Form->control('ri_item_serial_number', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Contact No</label>
                <?php echo $this->Form->control('ri_contact_number', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Due On</label>
                <?php echo $this->Form->control('ri_due_on', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-md-6 m-t-15">Standing Queu <small>(Specify Number)</small></label>
                <?php echo $this->Form->control('ri_standing_que', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <h5 class="card-title text-center formTitle"><b>2: ICT SERVICE/s REQUESTED</b></h5>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-md-6 m-t-15">Description of the Problem <small>(Optional)</small></label>
                <?php echo $this->Form->control('is_description_of_problem', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Recomended Solution</label>
                <?php echo $this->Form->control('is_recomended_solution', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Particulars</label>
                <?php echo $this->Form->control('is_particular', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-md-6 m-t-15">Other Services <small>(Please Specify)</small></label>
                <?php echo $this->Form->control('is_other_services', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Assigned ICT Personel</label>
                <?php echo $this->Form->control('is_assigned_ict_personel', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Expected Finish Date/Time</label>
                <?php echo $this->Form->control('is_expected_finish', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <h5 class="card-title text-center formTitle"><b>3: Service Report <small>(To be filled up by MIS Servicing Staff)</small></b></h5>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Date of the Accomplishment of Service</label>
                <?php echo $this->Form->control('sr_accomplish_date', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Time</label>
                <?php echo $this->Form->control('sr_time', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Findings</label>
                <?php echo $this->Form->control('sr_finding', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Others</label>
                <?php echo $this->Form->control('sr_findings_others', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <h5 class="card-title text-center formTitle"><b>4: Service Assessment <small>(To be filled up by Client)</small></b></h5>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Date Received</label>
                <?php echo $this->Form->control('sa_date_recieved', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Time</label>
                <?php echo $this->Form->control('sa_time', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <small> Please objectively race our service performance on this specific service receive. Thank you</small>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-md-6 m-t-15">(Please check the appropriate box)</label>
                <?php echo $this->Form->control('sa_check_appropriate', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-md-6 m-t-15">Remarks</label>
                <?php echo $this->Form->control('sa_remarks', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
        <h5 class="card-title text-center formTitle"><b>5: Equipment Release</b></h5>
        <small> This is to confirm <b>RECEIPT</b> of the complete set of equipment indicated herein requested for fix.</small>
        <div class="form-group row">
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Date Received</label>
                <?php echo $this->Form->control('er_date_received', ['class' => 'form-control', 'label' => false]); ?>
            </div>
            <div class="col-md-6">
                <label class="col-md-6 m-t-15">Time</label>
                <?php echo $this->Form->control('er_remarks', ['class' => 'form-control', 'label' => false]); ?>
            </div>
        </div>
    </div>
    <div class="border-top">
        <div class="card-body">
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
    
</script>
