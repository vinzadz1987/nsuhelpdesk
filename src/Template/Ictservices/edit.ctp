<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ictservice $ictservice
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ictservice->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ictservice->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ictservices'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ictservices form large-9 medium-8 columns content">
    <?= $this->Form->create($ictservice) ?>
    <fieldset>
        <legend><?= __('Edit Ictservice') ?></legend>
        <?php
            echo $this->Form->control('ri_requesting_office_personel');
            echo $this->Form->control('ri_college_unit');
            echo $this->Form->control('ri_request_date');
            echo $this->Form->control('ri_time_received');
            echo $this->Form->control('ri_receiving_ict_heldesk_personel');
            echo $this->Form->control('ri_item_received');
            echo $this->Form->control('ri_item_serial_number');
            echo $this->Form->control('ri_contact_number');
            echo $this->Form->control('ri_due_on');
            echo $this->Form->control('ri_standing_que');
            echo $this->Form->control('is_description_of_problem');
            echo $this->Form->control('is_recomended_solution');
            echo $this->Form->control('is_other_services');
            echo $this->Form->control('is_assigned_ict_personel');
            echo $this->Form->control('is_endorsement');
            echo $this->Form->control('sr_accomplish_date');
            echo $this->Form->control('sr_time');
            echo $this->Form->control('sr_finding');
            echo $this->Form->control('sr_findings_others');
            echo $this->Form->control('sa_date_recieved');
            echo $this->Form->control('sa_time');
            echo $this->Form->control('sa_check_appropriate');
            echo $this->Form->control('sa_remarks');
            echo $this->Form->control('er_date_received');
            echo $this->Form->control('er_remarks');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
