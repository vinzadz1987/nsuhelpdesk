<div class="card">
    <div class="card-body">
        <h5 class="card-title">List ICT Request</h5>
        <div style="float: right; margin-bottom: 15px">
            <?php
                echo $this->Html->link(
                '<i class="fas fa-plus"></i> ICT Services',
                [
                    'controller' => 'Ictservices',
                    'action' => 'add'
                ],
                [ 
                    'escape' => false,
                    'class' => 'btn btn-primary btn-md'
                ]
                )
            ?>
        </div>
         <div style="float: right; margin-bottom: 15px; margin-right: 15px">
            <?php
                echo $this->Html->link(
                '<i class="fas fa-plus"></i> SIA Services',
                [
                    'controller' => 'Communications',
                    'action' => 'add'
                ],
                [ 
                    'escape' => false,
                    'class' => 'btn btn-info btn-md'
                ]
                )
            ?>
        </div>
        <div class="table-responsive">
            <table id="ictrequest_table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th><h2 style="color: Orange">1</h2>Requesting Office/Personel<br><small>REQUESTER INFORMATION</small></th>
                        <th>College/Unit<br><small>REQUESTER INFORMATION</small></th>
                        <th>Date<br><small>REQUESTER INFORMATION</small></th>
                        <th>Time Received<br><small>REQUESTER INFORMATION</small></th>
                        <th>Receiving ICT Helpdesk Personel<br><small>REQUESTER INFORMATION</small></th>
                        <th>Item received<br><small>REQUESTER INFORMATION</small></th>
                        <th>Item serial number<br><small>REQUESTER INFORMATION</small></th>
                        <th>Contact No<br><small>REQUESTER INFORMATION</small></th>
                        <th>Due On<br><small>REQUESTER INFORMATION</small></th>
                        <th>Standing Que<br><small>REQUESTER INFORMATION</small></th>
                        <th><h2 style="color: red">2</h2>Assigned ICT Personel<br><small>ICT SERVICES/REQUESTED</small></th>
                        <th>Endorsement<br><small>ICT SERVICES/REQUESTED</small></th>
                        <th><h2 style="color: blue">3</h2>Accomplish Date<br><small>Service Report</small></th>
                        <th>Time<br><small>Service Report</small></th>
                        <th>Finding<br><small>Service Report</small></th>
                        <th>Other Findings<br><small>Service Report</small></th>
                        <th><h2 style="color: green">4</h2>Date Recieved<br><small>Service Assesstment</small></th>
                        <th>Time<br><small>Service Assesstment</small></th>
                        <th>Check Appropriate<br><small>Service Assesstment</small></th>
                        <th>Remarks<br><small>Service Assesstment</small></th>
                        <th><h2 style="color: blue">5</h2>Date Received<br><small>Equipment release</small></th>
                        <th>Remarks<br><small>Equipment release</small></th>
                        <!-- <th>Item serial number</th> -->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ictservices as $ictservice): ?>
                    <tr>
                        <!-- <td><?= $this->Number->format($ictservice->id) ?></td> -->
                        <td><?= h($ictservice->ri_requesting_office_personel) ?></td>
                        <td><?= h($ictservice->ri_college_unit) ?></td>
                        <td><?= h($ictservice->ri_request_date) ?></td>
                        <td><?= h($ictservice->ri_time_received) ?></td>
                        <td><?= h($ictservice->ri_receiving_ict_heldesk_personel) ?></td>
                        <td><?= h($ictservice->ri_item_received) ?></td>
                        <td><?= $this->Number->format($ictservice->ri_item_serial_number) ?></td>
                        <td><?= $this->Number->format($ictservice->ri_contact_number) ?></td>
                        <td><?= h($ictservice->ri_due_on) ?></td>
                        <td><?= h($ictservice->ri_standing_que) ?></td>
                        <td><?= h($ictservice->is_assigned_ict_personel) ?></td>
                        <td><?= h($ictservice->sr_accomplish_date) ?></td>
                        <td><?= h($ictservice->is_endorsement) ?></td>
                        <td><?= h($ictservice->sr_time) ?></td>
                        <td><?= $this->Number->format($ictservice->sr_finding) ?></td>
                        <td><?= h($ictservice->sr_findings_others) ?></td>
                        <td><?= $this->Number->format($ictservice->sa_date_recieved) ?></td>
                        <td><?= h($ictservice->sa_time) ?></td>
                        <td><?= $this->Number->format($ictservice->sa_check_appropriate) ?></td>
                        <td><?= h($ictservice->sa_remarks) ?></td>
                        <td><?= h($ictservice->er_date_received) ?></td>
                        <td><?= h($ictservice->er_remarks) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $ictservice->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ictservice->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ictservice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ictservice->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><span id="view_title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="loadingAnimated" id="view_content"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ictservice'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ictservices index large-9 medium-8 columns content">
    <h3><?= __('Ictservices') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table table-bordered table-striped table-responsive">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('requesting_office_personel') ?></th>
                <th scope="col"><?= $this->Paginator->sort('college_unit') ?></th>
                <th scope="col"><?= $this->Paginator->sort('request_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('time_received') ?></th>
                <th scope="col"><?= $this->Paginator->sort('receiving_ict_heldesk_personel') ?></th>
                <th scope="col"><?= $this->Paginator->sort('item_received') ?></th>
                <th scope="col"><?= $this->Paginator->sort('item_serial_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contact_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('due_on') ?></th>
                <th scope="col"><?= $this->Paginator->sort('standing_que') ?></th>
                <th scope="col"><?= $this->Paginator->sort('assigned_ict_personel') ?></th>
                <th scope="col"><?= $this->Paginator->sort('endorsement') ?></th>
                <th scope="col"><?= $this->Paginator->sort('accomplish_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('time') ?></th>
                <th scope="col"><?= $this->Paginator->sort('finding') ?></th>
                <th scope="col"><?= $this->Paginator->sort('findings_others') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_recieved') ?></th>
                <th scope="col"><?= $this->Paginator->sort('time') ?></th>
                <th scope="col"><?= $this->Paginator->sort('check_appropriate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('remarks') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_received') ?></th>
                <th scope="col"><?= $this->Paginator->sort('remarks') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ictservices as $ictservice): ?>
            <tr>
                <td><?= $this->Number->format($ictservice->id) ?></td>
                <td><?= h($ictservice->ri_requesting_office_personel) ?></td>
                <td><?= h($ictservice->ri_college_unit) ?></td>
                <td><?= h($ictservice->ri_request_date) ?></td>
                <td><?= h($ictservice->ri_time_received) ?></td>
                <td><?= h($ictservice->ri_receiving_ict_heldesk_personel) ?></td>
                <td><?= h($ictservice->ri_item_received) ?></td>
                <td><?= $this->Number->format($ictservice->ri_item_serial_number) ?></td>
                <td><?= $this->Number->format($ictservice->ri_contact_number) ?></td>
                <td><?= h($ictservice->ri_due_on) ?></td>
                <td><?= h($ictservice->ri_standing_que) ?></td>
                <td><?= h($ictservice->is_assigned_ict_personel) ?></td>
                <td><?= h($ictservice->is_endorsement) ?></td>
                <td><?= h($ictservice->sr_accomplish_date) ?></td>
                <td><?= h($ictservice->sr_time) ?></td>
                <td><?= $this->Number->format($ictservice->sr_finding) ?></td>
                <td><?= h($ictservice->sr_findings_others) ?></td>
                <td><?= $this->Number->format($ictservice->sa_date_recieved) ?></td>
                <td><?= h($ictservice->sa_time) ?></td>
                <td><?= $this->Number->format($ictservice->sa_check_appropriate) ?></td>
                <td><?= h($ictservice->sa_remarks) ?></td>
                <td><?= h($ictservice->er_date_received) ?></td>
                <td><?= h($ictservice->er_remarks) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ictservice->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ictservice->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ictservice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ictservice->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div> -->
