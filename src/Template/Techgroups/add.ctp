
<div class="techgroups form large-9 medium-8 columns content">
    <?= $this->Form->create($techgroup) ?>
    <fieldset>
        <legend><?= __('Add Techgroup') ?></legend>
        <?php
            echo $this->Form->control('name', ['class' => 'form-control']);
            echo $this->Form->control('description', ['class' => 'form-control']);
            echo $this->Form->control('icon', ['class' => 'form-control']);
        ?>
    </fieldset>
    <hr>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Html->link(__('Back'), ['action' => 'index'], ['class' => 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>
