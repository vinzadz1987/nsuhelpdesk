
<div class="techgroups index large-9 medium-8 columns content">
    <h3><?= __('Techgroups') ?></h3>
    <?= $this->Html->link(__('New Techgroup'), ['action' => 'add'],['class' => 'btn btn-primary']) ?>
    <table class="table" id="techgroupTable">
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('icon') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($techgroups as $techgroup): ?>
            <tr>
                <!-- <td><?= $this->Number->format($techgroup->id) ?></td> -->
                <td><?= h($techgroup->name) ?></td>
                <td><?= h($techgroup->icon) ?></td>
                <td class="actions">
                    <!-- <?= $this->Html->link(__('View'), ['action' => 'view', $techgroup->id]) ?> -->
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $techgroup->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $techgroup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $techgroup->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
