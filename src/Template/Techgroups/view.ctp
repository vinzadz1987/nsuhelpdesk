<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Techgroup $techgroup
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Techgroup'), ['action' => 'edit', $techgroup->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Techgroup'), ['action' => 'delete', $techgroup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $techgroup->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Techgroups'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Techgroup'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="techgroups view large-9 medium-8 columns content">
    <h3><?= h($techgroup->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($techgroup->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Icon') ?></th>
            <td><?= h($techgroup->icon) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($techgroup->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($techgroup->description)); ?>
    </div>
</div>
