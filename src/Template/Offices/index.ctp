
<div class="offices index large-9 medium-8 columns content" style="background-color:white; padding: 20px ">
    <h3><?= __('Offices') ?></h3>
    <?= $this->Html->link(__('New Office'), ['action' => 'add'],['class' => 'btn btn-primary']) ?>
    <table id="officesTable" class="table">
        <thead>
            <tr>
                <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($offices as $office): ?>
            <tr>
                <!-- <td><?= $this->Number->format($office->id) ?></td> -->
                <td><?= h($office->name) ?></td>
                <td><?= h($office->description) ?></td>
                <td class="actions">
                   <!--  <?= $this->Html->link(__('View'), ['action' => 'view', $office->id]) ?> -->
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $office->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $office->id], ['confirm' => __('Are you sure you want to delete # {0}?', $office->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
