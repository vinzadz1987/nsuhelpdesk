
<div class="technicals index large-9 medium-8 columns content" style="background: white; padding: 10px">
    <h3><?= __('Technicals') ?></h3>
    <?= $this->Html->link(__('New Technical'), ['action' => 'add'],['class' => 'btn btn-primary']) ?>
    <table id="technicalsTable" class="table">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('name') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('groups') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($technicals as $technical): ?>
            <tr>
                <td><?= $this->Number->format($technical->id) ?></td>
                <!-- <td><?= $users[h($technical->name)] ?></td> -->
                <td><?php 
                            if($technical->groups !== 0) {
                                echo $techgroups[$this->Number->format($technical->groups)];
                            }
                        ?>  </td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $technical->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $technical->id], ['confirm' => __('Are you sure you want to delete # {0}?', $technical->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
