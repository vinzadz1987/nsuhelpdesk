<div class="technicals form large-9 medium-8 columns content" style="background: white; padding: 10px">
    <?= $this->Form->create($technical) ?>
    <fieldset>
        <legend><?= __('Update Technical') ?></legend>
        <?= $this->Form->control('name', [
            'options' => $users,
            'empty' => 'Please Select',
            'class' => ['select2 form-control']
        ]) ?> 
         <?= $this->Form->control('groups', [
            'options' => $techgroups,
            'empty' => 'Please Select',
            'class' => ['select2 form-control']
        ]) ?> 
        <?php
            echo $this->Form->control('description',['class' => 'form-control']);
        ?>
    </fieldset>
    <hr>
    <?= $this->Form->button(__('Save'),['class' => 'btn btn-primary']) ?>.
    <?= $this->Html->link(__('Back'), ['action' => 'index'],['class' => 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>
