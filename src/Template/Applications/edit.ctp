<div class="card">
	<?= $this->Form->create($application, ['class' => 'form-horizontal', 'type' => 'file']) ?>
		<div class="card-body">
			<h4 class="card-title">Edit Widget</h4>
			<div class="form-group row">
				<label for="fname" class="col-sm-3 text-right control-label col-form-label">Title*</label>
				<div class="col-sm-9">
					<?php echo $this->Form->control('name', ['label' => false, 'class' => 'form-control']) ?>
				</div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">URL*</label>
				<div class="col-sm-9">
					<?= $this->Form->control('url', ['class' => 'form-control', 'label' => false]) ?>
					<small>(e.g: http://www.example.com)</small>
				</div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">Widget Image*</label>
				<div class="col-md-9">
					<div class="custom-file">
						<?= $this->Form->control('icon', ['class' => 'custom-file-input', 'type' => 'file', 'label' => false, 'value' => $application['icon'], 'accept' => 'image/*']) ?>
						<label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
						<small>File exceeds the maximum allowed size (2MB)</small>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="cono1" class="col-sm-3 text-right control-label col-form-label">Description*</label>
				<div class="col-sm-9">
					<?= $this->Form->textarea('description', ['class' => 'form-control']) ?>
				</div>
			</div>
		</div>
		<div class="border-top">
			<div class="card-body">
				<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>

