<div class="card">
    <div class="card-body">
        <h5 class="card-title">WIDGET MANAGEMENT</h5>
        <div style="float: right; margin-bottom: 15px">
            <?php
                echo $this->Html->link(
                '<i class="fas fa-plus"></i> Add Widget',
                [
                    'controller' => 'Applications',
                    'action' => 'add'
                ],
                [ 
                    'escape' => false,
                    'class' => 'btn btn-primary'
                ]
                )
            ?>
        </div>
        <div class="table-responsive">
            <table id="widgetList" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                        <th>Title</th>
                        <th>URL of Projects</th>
                        <!-- <th>Location</th> -->
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($applications as $application): ?>
                    <tr>
                        <td></td>
                        <td><?= h($application->name) ?></td>
                        <td><?= h($application->url) ?></td>
                        <!-- <td></td> -->
                        <td>Status</td>
                        <td><?= $this->Html->link(__('Edit'), ['action' => 'edit', $application->id]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><span id="view_title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="loadingAnimated" id="view_content"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script(['libs/jquery/dist/jquery.min','communication']) ?>