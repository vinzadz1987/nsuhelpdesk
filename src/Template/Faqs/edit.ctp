
<div class="card">
    <div class="card-body">
        <div class="faqs form large-9 medium-8 columns content">
            <?= $this->Form->create($faq) ?>
            <fieldset>
                <legend><?= __('Update Faq') ?></legend>
                <?php
                    echo $this->Form->control('title', ['class' => 'form-control']);
                    echo $this->Form->control('message', ['class' => 'form-control']);
                ?>
            </fieldset>
            <hr>
            <?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class' => 'btn btn-primary', 'escape' => false]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="row">
    <?php foreach ($faqs as $faq): ?>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?= ucfirst(h($faq->title)) ?></h5>
                    <p><?= ucfirst(h($faq->message)) ?></p>
                    <span class="float-right"><small><?= h($faq->created) ?></small></span>

                    <?= $this->Html->link(__('<i class="fa fa-pencil"></i>'), ['action' => 'edit', $faq->id], ['escape' => false]) ?>
                    <?= $this->Form->postLink(__('<i class="fa fa-trash"></i>'), ['action' => 'delete', $faq->id], ['confirm' => __('Are you sure you want to delete # {0}?', $faq->id), 'escape' => false, 'class' => 'text-danger']) ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>