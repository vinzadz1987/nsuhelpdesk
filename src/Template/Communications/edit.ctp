<div class="card">
	<div class="card-body">
		<?= $this->Form->postLink(
			__('Delete'),
			['action' => 'delete', $communication->id],
			['confirm' => __('Are you sure you want to delete # {0}?', $communication->id), 'class' => 'btn btn-danger pull-left']
		)
		?>
	</div>
    <?= $this->Form->create($communication, ['class' => 'form-horizontal']) ?>
        <div class="card-body">
            <h4 class="card-title">Edit Communication</h4>
            <div class="form-group row">
                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Communication Type</label>
                <div class="col-sm-9">
                    <?php
                        echo $this->Form->select('communication_type', $communication_type, ['class' => 'form-control', 'empty' => true]);
                    ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Author</label>
                <div class="col-sm-9">
                    <?= $this->Form->control('author', ['placeholder' => 'Complete Name', 'class' => 'form-control', 'label' => false]) ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Start Date</label>
                <div class="col-sm-9">
                    <?= $this->Form->control('start_date', ['class' => 'form-control', 'type' => 'text', 'value' => date('Y-m-d'), 'label' => false]) ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Stop Date</label>
                <div class="col-sm-9">
                    <?= $this->Form->control('stop_date', ['class' => 'form-control', 'type' => 'text', 'label' => false]) ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Heading</label>
                <div class="col-sm-9">
                    <?=  $this->Form->control('heading', ['class' => 'form-control', 'label' => false]) ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Message</label>
                <div class="col-sm-9">
                    <?= $this->Form->textarea('message', ['class' => 'form-control']) ?>
                </div>
            </div>
        </div>
		<div class="border-top">
			<div class="card-body">
				<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
    <?= $this->Form->end() ?>
</div>

