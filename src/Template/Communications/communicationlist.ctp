<div class="card">
	<div class="card-body">
		<h5 class="card-title">List Communications</h5>
		<div style="float: right; margin-bottom: 15px">
			<?php
				echo $this->Html->link(
				'<i class="fas fa-plus"></i> Add Communication',
				[
					'controller' => 'Communications',
					'action' => 'add'
				],
				[ 
					'escape' => false,
					'class' => 'btn btn-primary btn-md'
				]
				)
			?>
		</div>
		<div class="table-responsive">
			<div id="realtimeCommunication"></div>
			<table id="communication_table" class="table">
				<thead>
					<tr>
						<th style="width: 200px">Communication Type</th>
						<th>Author</th>
						<th>Heading</th>
						<th>Message</th>
						<th>Likes</th>
						<th>Comments</th>
						<th>Release</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($communicationlist as $communication): ?>
					<tr style="<?= h($communication->status == 1)? 'font-weight: bold' : ''; ?>">
						<td><?= $communication_type[$communication->communication_type] ?></td>
						<td><?= ucfirst(h($communication->author)) ?></td>
						<td><?= ucfirst(h($communication->heading)) ?></td>
						<td>
							<button class="btn btn-sm btn-light" data-id="<?= $communication->id ?>" data-toggle="modal" data-target="#modal<?= $communication->id ?>" onclick="showComment(this, 1)"><i class="fas fa-eye"></i> View</button>
							<div class="modal fade" id="modal<?= $communication->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">
										<?php echo ucfirst(h($communication->heading)); ?><br>
										<small class="text-muted"><?= $communication_type[$communication->communication_type] ?></small>
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<?= h($communication->message) ?><br>
									<span class="text-muted"><small>Release: <?= h($communication->created) ?></small></span>
								</div>
								<div class="modal-footer">
									<button class='btn btn-light btn-xs' data-id="<?= h($communication->id) ?>" onclick="showComment(this, 2)" data-toggle="popover"><i class='m-r-10 mdi mdi-comment'></i> Comment</button> <span class="badge badge-pill badge-success float-right coun countComment<?= h($communication->id) ?>">0</span>
									<button class='btn btn-light btn-xs' data-id="<?= h($communication->id) ?>" onclick='likeComment(this)'><i class='m-r-10 mdi mdi-thumb-up'></i> Like</button> <span class="badge badge-pill badge-info float-right">0</span>
								</div>
								<div class="commentBox" id="commentBox<?= $communication->id ?>" style="padding: 5px; display: none;">
									<div class="commentContent"></div>
									<input type="hidden" class="userid" name="userid" value="<?=$sess_user_id?>">
									<input type="hidden" class="username" name="username" value="<?=$sess_username?>">
									<textarea class="form-control commentText" placeholder="Please enter you comment"></textarea>
									<button class='btn btn-info btn-xs float-right postComment' data-id="<?= $communication->id ?>">Post</button>
								</div>
								<div id="commentHere<?= $communication->id ?>" style="display: none"></div>
							</div>
							</div>
						</div>
						</td>
						<td>
							<a href="javascript::void(0)"><span class="badge badge-pill badge-success">0</span></a>
						</td>
						<td>
							<a href="javascript::void(0)"><span class="badge badge-pill badge-info">0</span></a>
						</td>
						<td><?= h($communication->created) ?></td>
						<td class="actions">
							<?= $this->Html->link(__('Edit'), ['action' => 'edit', $communication->id]) ?>
							<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $communication->id], ['confirm' => __('Are you sure you want to delete # {0}?', $communication->id)]) ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><span id="view_title"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="loadingAnimated" id="view_content"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
