<div class="card">
	<?= $this->Form->create($communication, ['class' => 'form-horizontal']) ?>
		<div class="card-body">
			<h4 class="card-title">Add Communication</h4>
			<div class="form-group row">
				<label for="fname" class="col-sm-3 text-right control-label col-form-label">Communication Type</label>
				<div class="col-sm-9">
					<?php
						echo $this->Form->select('communication_type', $communication_type, ['class' => 'form-control', 'empty' => true]);
					?>
				</div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">Author</label>
				<div class="col-sm-9">
					<?= $this->Form->control('author', ['class' => 'form-control', 'label' => false]) ?>
				</div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">Start Date</label>
				<div class="col-sm-9">
					<?= $this->Form->control('start_date', ['class' => 'form-control', 'type' => 'text', 'value' => date("Y-m-d"), 'label' => false]) ?>
				</div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">Stop Date</label>
				<div class="col-sm-9">
					<?= $this->Form->control('stop_date', ['class' => 'form-control', 'type' => 'text', 'label' => false]) ?>
				</div>
			</div>
			<div class="form-group row">
				<label for="cono1" class="col-sm-3 text-right control-label col-form-label">Heading</label>
				<div class="col-sm-9">
					<?=  $this->Form->control('heading', ['class' => 'form-control', 'label' => false]) ?>
				</div>
			</div>
			<div class="form-group row">
				<label for="cono1" class="col-sm-3 text-right control-label col-form-label">Message</label>
				<div class="col-sm-9">
					<?= $this->Form->textarea('message', ['class' => 'form-control']) ?>
				</div>
			</div>
		</div>
		<br>
		<div class="border-top">
			<div class="card-body">
				<?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
	<?= $this->Form->end() ?>
</div>
<?= $this->Html->script(['libs/jquery/dist/jquery.min']) ?>
<script type="text/javascript">
	$(function () {
		$("form").on("submit", function( event) {
			event.preventDefault();
			$.ajax({
				type: 'post',
				async: true,
				cache: false,
				url:'add',
				success: function (response) {
					var pj = $.parseJSON(response);
					if(pj.result == "success") {
						var typeData = { broadType : Broadcast.POST, data : pj.communication_data };
						connection.sendDataCommunication(typeData);
						toastr.success('Added communication succesfully.','Success');
					} else {
						toastr.error('Something error in adding communication.', 'Error!');
					}
				},
				error: function(response) {
					toastr.error('Something error in adding communication.', 'Error!');
				},
				data: $("form").serialize()
			});
		});
	});
</script>