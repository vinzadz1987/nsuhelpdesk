<div class="card">
	<div class="card-body">
		<?= $this->Html->link(__('Edit'), ['action' => 'edit', $communication->id], ['class' => 'btn btn-primary']) ?>
		<?= $this->Html->link(__('New Communication'), ['action' => 'add'], ['class' => 'btn btn-primary']) ?>
		<?= $this->Html->link(__('List Communications'), ['action' => 'communicationlist'], ['class' => 'btn btn-info']) ?>
		<?= $this->Form->postLink(
			__('Delete'),
			['action' => 'delete', $communication->id],
			['confirm' => __('Are you sure you want to delete # {0}?', $communication->id), 'class' => 'btn btn-danger']
		)
		?>
	</div>
		<div class="card-body">
			<h4 class="card-title">View Communication</h4>
			<div class="form-group row">
				<label for="fname" class="col-sm-3 text-right control-label col-form-label">Communication Type</label>
				<div class="col-sm-9">
					<?php
						if($communication->communication_type == 0) {
							echo 'General Communication';
						} else {
							echo 'Critical Communication';
						}
					?>
				</div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">Author</label>
				<div class="col-sm-9"><?= h($communication->author) ?></div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">Start Date</label>
				<div class="col-sm-9"><?= h($communication->start_date) ?></div>
			</div>
			<div class="form-group row">
				<label for="lname" class="col-sm-3 text-right control-label col-form-label">Stop Date</label>
				<div class="col-sm-9"><?= h($communication->stop_date) ?></div>
			</div>
			<div class="form-group row">
				<label for="cono1" class="col-sm-3 text-right control-label col-form-label">Heading</label>
				<div class="col-sm-9"><?= h($communication->heading) ?></div>
			</div>
			<div class="form-group row">
				<label for="cono1" class="col-sm-3 text-right control-label col-form-label">Message</label>
				<div class="col-sm-9"><?= $this->Text->autoParagraph(h($communication->message)); ?></div>
			</div>
		</div>
</div>
