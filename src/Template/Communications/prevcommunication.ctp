<div class="card">
	<div class="card-body">
		<h5 class="card-title">Previous Communications</h5>
		<div class="table-responsive">
			<table id="previous_communication" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style="width: 200px">Communication Type</th>
						<th>Author</th>
						<th>Heading</th>
						<th>Message</th>
						<th>Likes</th>
						<th>Comments</th>
						<th>Release</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($communicationlist as $communication): ?>
					<tr style="<?= h($communication->status == 1)? 'font-weight: bold' : ''; ?>">
						<td><?= $communication_type[$communication->communication_type] ?></td>
						<td><?= h($communication->author) ?></td>
						<td><?= h($communication->heading) ?></td>
						<td>
							<a tabindex="0"
								class="btn btn-sm btn-primary" 
								role="button" 
								data-html="true" 
								data-toggle="popover" 
								title="<b>Message</b>" 
								data-content="<?= h($communication->message) ?>">
								<i class="fas fa-eye"></i> View
							</a>
						</td>
						<td>
							<a tabindex="0"
								class="btn btn-sm btn-primary" 
								role="button" 
								data-html="true" 
								data-toggle="popover" 
								title="<b>Message</b>" 
								data-content="<?= h($communication->message) ?>">
								<i class="fas fa-eye"></i> View
							</a>
						</td>
						<td>
							<a tabindex="0"
								class="btn btn-sm btn-primary" 
								role="button" 
								data-html="true" 
								data-toggle="popover" 
								title="<b>Message</b>" 
								data-content="<?= h($communication->message) ?>">
								<i class="fas fa-eye"></i> View
							</a>
						</td>
						<td><?= h($communication->created) ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><span id="view_title"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="loadingAnimated" id="view_content"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?= $this->Html->script(['libs/jquery/dist/jquery.min','communication']) ?>



