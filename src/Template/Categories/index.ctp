
<div class="categories index large-9 medium-8 columns content" style="background-color:white; padding: 20px ">
    <h3><?= __('Services Types') ?></h3>
    <?= $this->Html->link(__('New Services'), ['action' => 'add'],['class' => 'btn btn-primary']) ?>
    <table class="table" id="CategoriesTable">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $category): ?>
            <tr>
                <td><?= $this->Number->format($category->id) ?></td>
                <td><?= h($category->name) ?></td>
                <td class="actions">
                   <!--  <?= $this->Html->link(__('View'), ['action' => 'view', $category->id]) ?> -->
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $category->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
