<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ictservice Entity
 *
 * @property int $id
 * @property string $ri_requesting_office_personel
 * @property string $ri_college_unit
 * @property \Cake\I18n\FrozenDate $ri_request_date
 * @property \Cake\I18n\FrozenTime $ri_time_received
 * @property string $ri_receiving_ict_heldesk_personel
 * @property string $ri_item_received
 * @property int $ri_item_serial_number
 * @property int $ri_contact_number
 * @property \Cake\I18n\FrozenDate $ri_due_on
 * @property string $ri_standing_que
 * @property string $is_description_of_problem
 * @property string $is_recomended_solution
 * @property string $is_particular
 * @property \Cake\I18n\FrozenTime $is_expected_finish
 * @property string $is_other_services
 * @property string $is_assigned_ict_personel
 * @property string $is_endorsement
 * @property string $sr_accomplish_date
 * @property string $sr_time
 * @property int $sr_finding
 * @property string $sr_findings_others
 * @property int $sa_date_recieved
 * @property string $sa_time
 * @property int $sa_check_appropriate
 * @property string $sa_remarks
 * @property string $er_date_received
 * @property string $er_remarks
 */
class Ictservice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ri_requesting_office_personel' => true,
        'ri_college_unit' => true,
        'ri_request_date' => true,
        'ri_time_received' => true,
        'ri_receiving_ict_heldesk_personel' => true,
        'ri_item_received' => true,
        'ri_item_serial_number' => true,
        'ri_contact_number' => true,
        'ri_due_on' => true,
        'ri_standing_que' => true,
        'is_description_of_problem' => true,
        'is_recomended_solution' => true,
        'is_particular' => true,
        'is_expected_finish' => true,
        'is_other_services' => true,
        'is_assigned_ict_personel' => true,
        'is_endorsement' => true,
        'sr_accomplish_date' => true,
        'sr_time' => true,
        'sr_finding' => true,
        'sr_findings_others' => true,
        'sa_date_recieved' => true,
        'sa_time' => true,
        'sa_check_appropriate' => true,
        'sa_remarks' => true,
        'er_date_received' => true,
        'er_remarks' => true
    ];
}
