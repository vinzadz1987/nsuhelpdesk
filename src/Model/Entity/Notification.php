<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Notification Entity
 *
 * @property int $id
 * @property int $noti_type
 * @property int $userid
 * @property int $status
 * @property int $service_provider
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\User $user_noti
 */
class Notification extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'noti_type' => true,
        'userid' => true,
        'status' => true,
        'service_provider' => true,
        'created' => true,
        'user_noti' => true
    ];
}
