<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ticket Entity
 *
 * @property int $id
 * @property int $department
 * @property int $assign_tech
 * @property int $tech_group
 * @property int $ticket_type
 * @property int $request_type
 * @property int $request_category
 * @property int $category_type
 * @property string $subject
 * @property string $description
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Ticket extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'requesting_office' => true,
        'requesting_personel' => true,
        // 'department' => true,
        // 'assign_tech' => true,
        'tech_group' => true,
        'ticket_type' => true,
        'request_type' => true,
        // 'request_category' => true,
        'subject' => true,
        'description' => true,
        'files' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
