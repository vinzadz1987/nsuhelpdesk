<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tickets Model
 *
 * @method \App\Model\Entity\Ticket get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ticket newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ticket[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ticket|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ticket|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ticket patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ticket[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ticket findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TicketsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tickets');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'requesting_personel',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('requesting_office')
            ->requirePresence('requesting_office', 'create')
            ->notEmpty('requesting_office');

        $validator
            ->integer('requesting_personel')
            ->requirePresence('requesting_personel', 'create')
            ->notEmpty('requesting_personel');

        $validator
            ->integer('department')
            // ->requirePresence('department', 'create')
            ->allowEmpty('department');

        $validator
            ->scalar('assign_tech')
            // ->requirePresence('assign_tech', 'create')
            ->allowEmpty('assign_tech');

        $validator
            ->integer('tech_group')
            ->requirePresence('tech_group', 'create')
            ->notEmpty('tech_group');

        $validator
            ->integer('ticket_type')
            ->requirePresence('ticket_type', 'create')
            ->notEmpty('ticket_type');

        $validator
            ->integer('request_type')
            ->requirePresence('request_type', 'create')
            ->notEmpty('request_type');

        // $validator
        //     ->integer('request_category')
        //     ->requirePresence('request_category', 'create')
        //     ->notEmpty('request_category');

        // $validator
        //     ->integer('category_type')
        //     ->requirePresence('category_type', 'create')
        //     ->notEmpty('category_type');

        $validator
            ->scalar('subject')
            ->maxLength('subject', 500)
            ->requirePresence('subject', 'create')
            ->notEmpty('subject');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

         $validator
            ->scalar('files')
            ->requirePresence('files', 'create')
            ->allowEmpty('files');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
