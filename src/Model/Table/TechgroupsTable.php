<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Techgroups Model
 *
 * @method \App\Model\Entity\Techgroup get($primaryKey, $options = [])
 * @method \App\Model\Entity\Techgroup newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Techgroup[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Techgroup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Techgroup|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Techgroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Techgroup[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Techgroup findOrCreate($search, callable $callback = null, $options = [])
 */
class TechgroupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('techgroups');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 500)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('icon')
            ->maxLength('icon', 100)
            ->requirePresence('icon', 'create')
            ->notEmpty('icon');

        return $validator;
    }
}
