<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ictservices Model
 *
 * @method \App\Model\Entity\Ictservice get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ictservice newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ictservice[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ictservice|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ictservice|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ictservice patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ictservice[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ictservice findOrCreate($search, callable $callback = null, $options = [])
 */
class IctservicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ictservices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('ri_requesting_office_personel')
            ->maxLength('ri_requesting_office_personel', 225)
            ->requirePresence('ri_requesting_office_personel', 'create')
            ->notEmpty('ri_requesting_office_personel');

        $validator
            ->scalar('ri_college_unit')
            ->maxLength('ri_college_unit', 225)
            ->requirePresence('ri_college_unit', 'create')
            ->notEmpty('ri_college_unit');

        $validator
            ->date('ri_request_date')
            ->requirePresence('ri_request_date', 'create')
            ->notEmpty('ri_request_date');

        $validator
            ->time('ri_time_received')
            ->requirePresence('ri_time_received', 'create')
            ->notEmpty('ri_time_received');

        $validator
            ->scalar('ri_receiving_ict_heldesk_personel')
            ->maxLength('ri_receiving_ict_heldesk_personel', 225)
            ->requirePresence('ri_receiving_ict_heldesk_personel', 'create')
            ->notEmpty('ri_receiving_ict_heldesk_personel');

        $validator
            ->scalar('ri_item_received')
            ->maxLength('ri_item_received', 225)
            ->requirePresence('ri_item_received', 'create')
            ->notEmpty('ri_item_received');

        $validator
            ->integer('ri_item_serial_number')
            ->requirePresence('ri_item_serial_number', 'create')
            ->notEmpty('ri_item_serial_number');

        $validator
            ->integer('ri_contact_number')
            ->requirePresence('ri_contact_number', 'create')
            ->notEmpty('ri_contact_number');

        $validator
            ->date('ri_due_on')
            ->requirePresence('ri_due_on', 'create')
            ->notEmpty('ri_due_on');

        $validator
            ->scalar('ri_standing_que')
            ->maxLength('ri_standing_que', 225)
            ->requirePresence('ri_standing_que', 'create')
            ->notEmpty('ri_standing_que');

        $validator
            ->scalar('is_description_of_problem')
            ->requirePresence('is_description_of_problem', 'create')
            ->notEmpty('is_description_of_problem');

        $validator
            ->scalar('is_recomended_solution')
            ->requirePresence('is_recomended_solution', 'create')
            ->notEmpty('is_recomended_solution');

        $validator
            ->scalar('is_particular')
            ->requirePresence('is_particular', 'create')
            ->notEmpty('is_particular');

        $validator
            ->dateTime('is_expected_finish')
            ->requirePresence('is_expected_finish', 'create')
            ->notEmpty('is_expected_finish');

        $validator
            ->scalar('is_other_services')
            ->requirePresence('is_other_services', 'create')
            ->notEmpty('is_other_services');

        $validator
            ->scalar('is_assigned_ict_personel')
            ->maxLength('is_assigned_ict_personel', 225)
            ->requirePresence('is_assigned_ict_personel', 'create')
            ->notEmpty('is_assigned_ict_personel');

        $validator
            ->scalar('is_endorsement')
            ->maxLength('is_endorsement', 225)
            ->requirePresence('is_endorsement', 'create')
            ->notEmpty('is_endorsement');

        $validator
            ->scalar('sr_accomplish_date')
            ->maxLength('sr_accomplish_date', 50)
            ->requirePresence('sr_accomplish_date', 'create')
            ->notEmpty('sr_accomplish_date');

        $validator
            ->scalar('sr_time')
            ->maxLength('sr_time', 50)
            ->requirePresence('sr_time', 'create')
            ->notEmpty('sr_time');

        $validator
            ->integer('sr_finding')
            ->requirePresence('sr_finding', 'create')
            ->notEmpty('sr_finding');

        $validator
            ->scalar('sr_findings_others')
            ->maxLength('sr_findings_others', 225)
            ->requirePresence('sr_findings_others', 'create')
            ->notEmpty('sr_findings_others');

        $validator
            ->integer('sa_date_recieved')
            ->requirePresence('sa_date_recieved', 'create')
            ->notEmpty('sa_date_recieved');

        $validator
            ->scalar('sa_time')
            ->maxLength('sa_time', 50)
            ->requirePresence('sa_time', 'create')
            ->notEmpty('sa_time');

        $validator
            ->integer('sa_check_appropriate')
            ->requirePresence('sa_check_appropriate', 'create')
            ->notEmpty('sa_check_appropriate');

        $validator
            ->scalar('sa_remarks')
            ->maxLength('sa_remarks', 225)
            ->requirePresence('sa_remarks', 'create')
            ->notEmpty('sa_remarks');

        $validator
            ->scalar('er_date_received')
            ->maxLength('er_date_received', 50)
            ->requirePresence('er_date_received', 'create')
            ->notEmpty('er_date_received');

        $validator
            ->scalar('er_remarks')
            ->maxLength('er_remarks', 50)
            ->requirePresence('er_remarks', 'create')
            ->notEmpty('er_remarks');

        return $validator;
    }
}
