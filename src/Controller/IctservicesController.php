<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ictservices Controller
 *
 * @property \App\Model\Table\IctservicesTable $Ictservices
 *
 * @method \App\Model\Entity\Ictservice[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IctservicesController extends AppController
{

   public function initialize(){
        parent::initialize();
        $this->viewBuilder()->setLayout('home');
        $this->loadComponent('RequestHandler');

        $page_title = 'SERVICES';
        $this->set('page_title', $page_title);

    }

    public function index()
    {
        $ictservices = $this->paginate($this->Ictservices);

        $this->set(compact('ictservices'));
    }

    /**
     * View method
     *
     * @param string|null $id Ictservice id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ictservice = $this->Ictservices->get($id, [
            'contain' => []
        ]);

        $this->set('ictservice', $ictservice);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ictservice = $this->Ictservices->newEntity();
        // var_dump($this->request->getData());
        if ($this->request->is('post')) {
            $ictservice = $this->Ictservices->patchEntity($ictservice, $this->request->getData());
            if ($this->Ictservices->save($ictservice)) {
                $this->Flash->success(__('The ictservice has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ictservice could not be saved. Please, try again.'));
        }
        $this->set(compact('ictservice'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Ictservice id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ictservice = $this->Ictservices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ictservice = $this->Ictservices->patchEntity($ictservice, $this->request->getData());
            if ($this->Ictservices->save($ictservice)) {
                $this->Flash->success(__('The ictservice has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ictservice could not be saved. Please, try again.'));
        }
        $this->set(compact('ictservice'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Ictservice id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ictservice = $this->Ictservices->get($id);
        if ($this->Ictservices->delete($ictservice)) {
            $this->Flash->success(__('The ictservice has been deleted.'));
        } else {
            $this->Flash->error(__('The ictservice could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

     public function isAuthorized($user)
    {

        $userRole = $this->Auth->user('role');

        if($userRole == "Admin") {
            $action = $this->request->getParam('action');

            if (
                in_array($action, [
                    'index',
                    'view',
                    'add',
                    'edit',
                    'delete'
                ])
            ) {
                return true;
            }
        } else {
            return false;
        }

        $id = $this->request->getParam('pass.0');
        return parent::isAuthorized($user);
    }
}
