<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Technicals Controller
 *
 * @property \App\Model\Table\TechnicalsTable $Technicals
 *
 * @method \App\Model\Entity\Technical[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TechnicalsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
            'index',
            'view',
            'add',
            'edit',
            'delete'
        ]);
        $this->viewBuilder()->setLayout('home');
    }
    
    public function index()
    {
        $technicals = $this->Technicals->find();

        $this->set('technicals',$technicals);
    }

    /**
     * View method
     *
     * @param string|null $id Technical id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $technical = $this->Technicals->get($id, [
            'contain' => []
        ]);

        $this->set('technical', $technical);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $technical = $this->Technicals->newEntity();
        if ($this->request->is('post')) {
            $technical = $this->Technicals->patchEntity($technical, $this->request->getData());
            if ($this->Technicals->save($technical)) {
                $this->Flash->success(__('The technical has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Service Provider could not be saved. Please, try again.'));
        }
        $this->set(compact('technical'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Technical id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $technical = $this->Technicals->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $technical = $this->Technicals->patchEntity($technical, $this->request->getData());
            if ($this->Technicals->save($technical)) {
                $this->Flash->success(__('The technical has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The technical could not be saved. Please, try again.'));
        }
        $this->set(compact('technical'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Technical id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $technical = $this->Technicals->get($id);
        if ($this->Technicals->delete($technical)) {
            $this->Flash->success(__('The technical has been deleted.'));
        } else {
            $this->Flash->error(__('The technical could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
