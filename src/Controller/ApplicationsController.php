<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ApplicationsController extends AppController
{

	public function initialize(){
		parent::initialize();
		$this->loadComponent('Paginator');
		$this->viewBuilder()->setLayout('home');
		$page_title = 'WIDGET';
		$this->set('page_title', $page_title);
	}

	public function index()
	{
		$applications = $this->paginate($this->Applications);
		$this->set(compact('applications'));
	}

	public function applicationlist()
	{
		$this->set('page_title', 'DASHBOARD');

		$applications = TableRegistry::get('Applications')->find();
		$settings = ['limit' => 1,'maxLimit' => 100];
		$communicationlist = $this->paginate($this->Communications->find()->where(['status' => 1]), $settings);

		$this->set('applications', $applications);
		$this->set(compact('communicationlist'));
	}

    public function view($id = null)
    {
        $application = $this->Applications->get($id, [
            'contain' => []
        ]);

        $this->set('application', $application);
    }

	public function add()
	{
		$application = $this->Applications->newEntity();
		if ($this->request->is('post')) {
			if(!empty($this->request->data['icon']['name'])){
				$fileName = $this->request->data['icon']['name'].''.time();
				$uploadPath = 'widgets/';
				$uploadFile = $uploadPath.$fileName;
				if(move_uploaded_file($this->request->data['icon']['tmp_name'],$uploadFile)){
					$this->request->data['icon'] = $uploadFile;
				}
			}
			$application = $this->Applications->patchEntity($application, $this->request->getData());
			if ($this->Applications->save($application)) {
				$this->Flash->success(__('The application has been saved.'));

				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('The application could not be saved. Please, try again.'));
		}
		$this->set(compact('application'));
	}

    public function edit($id = null)
    {
        $application = $this->Applications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
        	if(!empty($this->request->data['icon']['name'])){
				$fileName = $this->request->data['icon']['name'];
				$uploadPath = 'widgets/'.time();
				$uploadFile = $uploadPath.$fileName;
				if(move_uploaded_file($this->request->data['icon']['tmp_name'],$uploadFile)){
					$this->request->data['icon'] = $uploadFile;
				}
			}
            $application = $this->Applications->patchEntity($application, $this->request->getData());
            if ($this->Applications->save($application)) {
                $this->Flash->success(__('The application has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The application could not be saved. Please, try again.'));
        }
        $this->set(compact('application'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $application = $this->Applications->get($id);
        if ($this->Applications->delete($application)) {
            $this->Flash->success(__('The application has been deleted.'));
        } else {
            $this->Flash->error(__('The application could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function acknowledge()
	{
		$this->autoRender = false;
		if ($this->request->is('post')) {
			$commTable = TableRegistry::get('Communications');
			$data = $this->request->getData();
			var_dump($data);
			$comm = $commTable->get($data['id']);
			$comm->status = 2;
			if($commTable->save($comm)) {
				return $this->redirect(['action' => 'applicationlist']);
			}
		}
	}

	public function isAuthorized($user)
	{
		$userRole = $this->Auth->user('role');
		if($userRole == "Admin") {
			$action = $this->request->getParam('action');
			if (
				in_array($action, [
					'index',
					'add',
					'edit',
					'view',
					'applicationlist',
					'delete',
					'acknowledge'
				])
			) {
				return true;
			}
		}
		 else {
			$action = $this->request->getParam('action');
			if (
				in_array($action, [
					'applicationlist',
					'acknowledge'
				])
			) {
				return true;
			}
			return false;
		}
	}
}
