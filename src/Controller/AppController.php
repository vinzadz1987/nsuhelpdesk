<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Communications');
        $this->loadModel('Comments');
        $this->loadModel('Users');
        $this->loadModel('Offices');
        $this->loadModel('Departments');
        $this->loadModel('Techgroups');
        $this->loadModel('Services');
        $this->loadModel('Categories');
        $this->loadModel('Role');
        $this->loadModel('Notifications');

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        // $this->loadComponent('Param');
        $this->loadComponent('Auth', [
            'authorize' => 'Controller', 
        	'authenticate' => [
        		'Form' => [
        			'fields' => [
        				'username' => 'email',
                        'password' => 'password',
                        // 'role' => 'role'
        			]
        		]
        	],
			'loginAction' => [
				'controller' => 'Users',
				'action' => 'login'
			],
			'loginRedirect' => [
				'controller' => 'Tickets',
				'action' => 'index'
			],
        	'unauthorizedRedirect' => [
                'controller' => 'Tickets',
                'action' => 'index'
            ],
        ]);
		$this->Auth->allow(['display']);
		$this->loadComponent('Security');
		$this->set('sess_username', $this->Auth->user('firstname').' '.$this->Auth->user('lastname'));
        $this->set('sess_user_id', $this->Auth->user('id'));
		$this->set('userRole', $this->Auth->user('role'));

		// problem checker
        if($this->Auth->user('role') == "User" ) {
            $checkIssues = $this->Communications->find()->where(['status' => 1])->count();
        } else {
            $checkIssues = 0;
        }

        $users = $this->Users->find('list',['keyField' => 'id','valueField' => function ($row) {
            return $row['firstname'] . ' ' . $row['lastname'];
        }])->toArray();
        $techgroups = $this->Techgroups->find('list',['keyField' => 'id','valueField' => 'name'])->toArray();
        $checkIssues = isset($checkIssues)? $checkIssues : 0; 
        $typeTable = ['<span class="badge badge-pill badge-warning">Critical</span>',
                        '<span class="badge badge-pill badge-danger">Urgent</span>',
                        '<span class="badge badge-pill badge-danger">High</span>',
                        '<span class="badge badge-pill badge-primary">Medium</span>',
                        '<span class="badge badge-pill badge-success">Low</span>'
                    ];
        $ticketTable = ['<span class="badge badge-pill badge-primary">New</span>',
                        '<span class="badge badge-pill badge-info">Open</span>',
                        '<span class="badge badge-pill badge-danger">Pending</span>',
                        '<span class="badge badge-pill badge-primary">In Progress</span>',
                        '<span class="badge badge-pill badge-success">Solved</span>',
                        '<span class="badge badge-pill badge-danger">Closed</span>'];
        $ticket_status = ['New','Open','Pending','In Progress','Solved','Closed'];
        $ticket_type = ['Critical','Urgent','High','Medium','Low'];
        $departments = $this->Departments->find('list',['keyField' => 'id', 'valueField' => 'name'])->toArray();
        $globalDepartment = $this->Departments->find('all')->toArray();
        $globalRoles = $this->Role->find('list',['keyField' => 'id', 'valueField' => 'name'])->toArray();


        $globalAuthUser = $this->Auth->user();
        $this->set(compact(
            'ticket_type',
            'ticket_status',
            'ticketTable',
            'communication_type',
            'typeTable',
            'users',
            'checkIssues',
            'techgroups',
            'departments',
            'globalRoles',
            'globalAuthUser',
            'globalDepartment'
        ));
    }

	public function isAuthorized($user)
	{
		return false;
	}

    public function authUser() {
        return $this->Auth->user();
    }
}
