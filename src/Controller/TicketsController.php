<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class TicketsController extends AppController
{

	public function initialize()
	{
		parent::initialize();
		$this->Auth->allow([
			'index',
			'view',
			'add',
			'edit',
			'history',
			'delete',
			'download',
			'dashboard',
			'addfaqs',
			'updateprofile',
            'providerdashboard',
            'userdashboard',
            'userslist',
            'adduser',
            'notifications'
		]);
		$this->loadComponent('RequestHandler');
		$this->viewBuilder()->setLayout('home');

		$offices = $this->Offices->find('list',['keyField' => 'id', 'valueField' => 'name'])->toArray();
		$techgroups = $this->Role->find('list',['keyField' => 'id', 'valueField' => 'name'])->where(['id not IN' => [1,4]])->toArray();
        $assignTech = $this->Users->find('list',['keyField' => 'id', 'valueField' => function ($row) {
            return $row['firstname'] . ' ' . $row['lastname'];
        }])->where(['role not IN' => [3,4]])->toArray();
		$services = $this->Services->find('list',['keyField' => 'id', 'valueField' => 'name'])->toArray();
		$serviceCategory = $this->Categories->find('list',['keyField' => 'id', 'valueField' => 'name'])->toArray();

		$this->set(compact('serviceCategory','services','techgroups','offices','departments', 'assignTech'));
	}

     public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['groups','add']);
        $actions = [
            'groups',
            'add'
        ];

        if (in_array($this->request->getParam('action'), $actions)) {
            $this->eventManager()->off($this->Csrf);
            $this->Security->config('unlockedActions', $actions);
        }
    }

	public function index()
	{
		if($this->Auth->user('role') == 4) {
            $this->redirect(['Controller' => 'tickets', 'action' => 'userdashboard']);
			$tickets = $this->Tickets->find()->contain(['Users'])->where(['requesting_personel' => $this->Auth->user('id')]);
		} else if($this->Auth->user('role') == 2) {
            $this->redirect(['Controller' => 'tickets', 'action' => 'providerdashboard']);
            $tickets = $this->Tickets->find()->contain(['Users'])->where(['tech_group' => $this->Auth->user('role')]);
        } else if($this->Auth->user('role') == 3) {
            $this->redirect(['Controller' => 'tickets', 'action' => 'providerdashboard']);
            $tickets = $this->Tickets->find()->contain(['Users'])->where(['tech_group' => $this->Auth->user('role')]);
        }
        else {
            $this->redirect(['Controller' => 'tickets', 'action' => 'dashboard']);
			$tickets = $this->Tickets->find()->contain(['Users']); 
		}
		$this->set('tickets', $tickets);
	}

	public function history()
	{
		if($this->Auth->user('role') == 4) {
            $tickets = $this->Tickets->find('all')->where(['requesting_personel' => $this->Auth->user('id') ]);
            // $tickets = $this->Tickets->find()->where(['requesting_personel' => $this->Auth->user('id') ]);
			// $tickets = $this->Tickets->find()->where(['requesting_personel' => $this->Auth->user('id'), 'status' => 5 ]);
		} else if ($this->Auth->user('role') == 2) {
			$tickets = $this->Tickets->find()->where(['tech_group' => 2]); 
		} else if ($this->Auth->user('role') == 3) {
            $tickets = $this->Tickets->find()->where(['tech_group' => 3]); 
        } else {
            $tickets = $this->Tickets->find()->where(['requesting_personel' => $this->Auth->user('id') ]);
        }
		$this->set('tickets', $tickets);
	}

    public function addfaqs() 
    {
    	$this->redirect(['Controller' => 'faqs', 'action' => 'add']);
    }

    public function view($id = null)
    {
        $ticket = $this->Tickets->get($id, [
            'contain' => []
        ]);

        $this->set('ticket', $ticket);
    }

    public function download($id=null) { 
        return $this->response->file(
            WWW_ROOT. $id,
            ['download' => true, 'name' => 'foo']
        );
    }

    public function userslist()
    {
        $users = $this->Users->find()->contain(['Departments']);

        $this->set(compact('users'));
    }

    public function userdashboard() {
        
    }

    public function dashboard() {
        
    }

    public function notifications() {
        $notif = $this->Notifications->find()->contain(['Users'])->contain(['Offices'])->where(['status' => 1, 'service_provider' => $this->Auth->user('role')]);

        

        $this->set(compact('notif'));
    }

    public function providerdashboard() {
        //Ticket type
        $ticket_type = $this->Tickets->find('all');

        $ticket_type->select([ 
            'ticket_type',
            'count' => $ticket_type->func()->count('*')
        ])
        ->where(['tech_group' => $this->Auth->user('role')])
        ->group('ticket_type');
        $ticketType = $ticket_type->toArray();
        
        //Status
        $status = $this->Tickets->find('all');

        $status->select([ 
            'status',
            'count' => $ticket_type->func()->count('*')
        ])
        ->where(['tech_group' => $this->Auth->user('role')])
        ->group('status');
        $status = $status->toArray();


        //Ticket type
        $Notifications = $this->Notifications->find('all');

        $Notifications->select([ 
            'status',
            'count' => $Notifications->func()->count('*')
        ])
        ->where(['service_provider' => $this->Auth->user('role')])
        ->group('status');
        $Notifications = $Notifications->toArray();

        $this->set(compact('ticketType','status', 'Notifications'));
    }

    public function add()
    {
        $ticket = $this->Tickets->newEntity();
        if ($this->request->is('post')) {
            if(!empty($this->request->data['files']['name'])){
                $fileName = $this->request->data['files']['name'];
                $uploadPath = 'files/';
                $uploadFile = $uploadPath.time().$fileName;
                if(move_uploaded_file($this->request->data['files']['tmp_name'],$uploadFile)){
                    $this->request->data['files'] = $uploadFile;
                }
            }

                $ticket = $this->Tickets->patchEntity($ticket, $this->request->getData());
                if ($this->Tickets->save($ticket)) {

                    $data = $this->request->getData();
                    $articlesTable = TableRegistry::get('Notifications');
                    $article = $articlesTable->newEntity();

                    $article->noti_type = $data['ticket_type'];
                    $article->userid = $this->Auth->user('id');
                    $article->status = 1;
                    $article->service_provider = $data['tech_group'];
                    $article->requesting_office = $data['tech_group'];
                    $articlesTable->save($article);

                    $this->Flash->success(__('The request has been sent.'));

                    return $this->redirect(['action' => 'index']);
                }
        
            $this->Flash->error(__('The ticket could not be saved. Please, try again.'));
        }
        $authDept = $this->Auth->user('department');
        $this->set(compact('ticket','authDept'));
    }

    public function adduser()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $getData = $this->request->getData();
            if($getData['password'] !== $getData['confirm_password']) {
                
                $this->Flash->error(__('Password do not match, try again.'));   
            } 
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been successfully registered.'));

                // return $this->redirect(['action' => 'login']);
                $this->redirect(['Controller' => 'tickets', 'action' => 'userslist']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
            // return $this->redirect(['action' => 'login']);
        }
        $this->set(compact('user'));
    }

    public function edit($id = null)
    {
        $ticket = $this->Tickets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($this->request->data['files']['name'])){
                $fileName = $this->request->data['files']['name'];
                $uploadPath = 'files/';
                $uploadFile = $uploadPath.time().$fileName;
                if(move_uploaded_file($this->request->data['files']['tmp_name'],$uploadFile)){
                    $this->request->data['files'] = $uploadFile;
                }
            }
            $ticket = $this->Tickets->patchEntity($ticket, $this->request->getData());
            if ($this->Tickets->save($ticket)) {
                $this->Flash->success(__('The ticket has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ticket could not be saved. Please, try again.'));
        }
        $this->set(compact('ticket'));
    }

	public function updateprofile($id = null) {
		$user = $this->Users->get($id, [
			'contain' => []
		]);
		if ($this->request->is(['patch', 'post', 'put'])) {
			if(!empty($this->request->data['photo']['name'])){
				$fileName = $this->request->data['photo']['name'];
				$uploadPath = 'photos/';
				$uploadFile = $uploadPath.time().$fileName;
				if(move_uploaded_file($this->request->data['photo']['tmp_name'],$uploadFile)){
					$this->request->data['photo'] = $uploadFile;
				}
			}
			$user = $this->Users->patchEntity($user, $this->request->getData());
			if ($this->Users->save($user)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'updateprofile',$id]);
			}
			$this->Flash->error(__('The user could not be saved. Please, try again.'));
		}
		$this->set(compact('user'));
	}


    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ticket = $this->Tickets->get($id);
        if ($this->Tickets->delete($ticket)) {
            $this->Flash->success(__('The ticket has been deleted.'));
        } else {
            $this->Flash->error(__('The ticket could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
