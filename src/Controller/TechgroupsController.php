<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Techgroups Controller
 *
 * @property \App\Model\Table\TechgroupsTable $Techgroups
 *
 * @method \App\Model\Entity\Techgroup[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TechgroupsController extends AppController
{

     public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
            'index',
            'view',
            'add',
            'edit',
            'delete'
        ]);
        $this->viewBuilder()->setLayout('home');
    }

    public function index()
    {
        $techgroups = $this->Techgroups->find();

        $this->set('techgroups',$techgroups);
    }

    /**
     * View method
     *
     * @param string|null $id Techgroup id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $techgroup = $this->Techgroups->get($id, [
            'contain' => []
        ]);

        $this->set('techgroup', $techgroup);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $techgroup = $this->Techgroups->newEntity();
        if ($this->request->is('post')) {
            $techgroup = $this->Techgroups->patchEntity($techgroup, $this->request->getData());
            if ($this->Techgroups->save($techgroup)) {
                $this->Flash->success(__('The techgroup has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The techgroup could not be saved. Please, try again.'));
        }
        $this->set(compact('techgroup'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Techgroup id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $techgroup = $this->Techgroups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techgroup = $this->Techgroups->patchEntity($techgroup, $this->request->getData());
            if ($this->Techgroups->save($techgroup)) {
                $this->Flash->success(__('The techgroup has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The techgroup could not be saved. Please, try again.'));
        }
        $this->set(compact('techgroup'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Techgroup id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techgroup = $this->Techgroups->get($id);
        if ($this->Techgroups->delete($techgroup)) {
            $this->Flash->success(__('The techgroup has been deleted.'));
        } else {
            $this->Flash->error(__('The techgroup could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
